1. User submits KYC document via a web form
2. Web form automatically encrypts document to a key held in KMS (with offline backups)
3. Encrypted documents are submitted to an API gateway hook that triggers a lambda job which places the documents directly into an s3 bucket.
4. A support agent opens the KYC review interface and clicks a document to decrypt.
5. The support agent browser automatically generates a random encryption public key, and public key and the ID of the requested document they wish to decrypt to API Gateway
6. API Gateway launches a lambda job which hashes the document request with a random challenge and returns it to the browser
7. The browser prompts the support agent to tap their Yubikey which signs the challenge.
8. The browser sends the signed challenge back to API Gateway.
9. API gateway passes the signed document request payload to a lambda job which has access to the KMS role to use the KYC decryption key.
10. Lambda job decrypts the one document, and then encrypts it to the encryption key of the request and returns it to the support agent browser.
11. Document is decrypted and displayed in support agent browser.
12. Agent reviews document and it is automatically deleted locally when closed.