## Multi-party Signed Git workflows

### Path 1

This path allows most devs to use the tools they are used to, but requires a second security-only review later

1. Author submits changes for review
2. Reviewer and author iterate on changes for style, quality, and functionality using any collaboration tool they wish
3. Reviewer merges changes with signed merge commit
4. Several cycles of steps 1-3 complete until it is time for a release
5. Release engineer tags a release candidate
6. Release candidate is tested in staging
7. Security reviewers do "git sig review" locally examining total diff from last release to current release candidate for any malicious code (ignoring style/quality/functionality)
8. Security reviewers do "git sig add" to attach a signed tags to release candidate
9. Release engineer adds signed release tag to release candidate making it eligible for production deployment

### Path 2
This path is shorter and more flexible, but will mean all parties need to learn how to use secure review tooling.

1. Author submits changes for review
2. Reviewer and author iterate on changes using any tool they wish
3. Author or reviewer merge changes with or without a merge commit
4. Reviewer does "git sig review" to bump their review marker to HEAD.

## Notes

* Reviews done in a third party WebUI defeat the point of local signing
  * You don't know if a force push happened between your review and local pull/sign/push
  * For general code-quality this may be tolerable, but review should not be trusted in a security context
* All changes must have a signature by the author and someone other than the author
  * This means in Path 1 that a security reviewer cannot also be a contributor to a given context
* "git sig review" tool /could/ support a folder argument
  * would signify a given review only applies to a given folder
  * will "git sig verify" will similarly need to be folder aware
  * Without this, trust on important apps like "signer" will not be practical in a monorepo.