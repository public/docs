## Web Content Signing via Service Workers
- Implementation:
  - M-of-n parties deterministically compile web interface bundle and sign it
  - Interface installs service worker mandates all future updates are
    - signed with m-of-n valid keys certified by a pinned CA
    - newer timestamp than current version
- Protections
  - Compromised insider tampering with frontends
  - BGP attacks
  - DNS takeover
  - TLS MITM
- Resources
  - https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API/Using_Service_Workers
  - https://arxiv.org/pdf/2105.05551

## Web Request Signing via WebAuthn
- Implementation:
  - Collect WebAuthn public keys for one or more devices for all users
   - External Authenticators: Yubikey, Nitrokey, Ledger, Trezor, Solokey, etc.
   - Platform Authenticators: iOS 13+, Android 7+, Windows Hello, many Chromebooks
  - Certify Webauthn public keys with trusted enclave
  - Webauthn sign all impacting web requests like trades and transfers
  - Private key enclaves validate request signatures before signing trades and transfers
- Protections:
  - Compromised insider tampering with backends
  - TLS MITM
- Resources:
  - https://developers.yubico.com/WebAuthn/Concepts/Using_WebAuthn_for_Signing.html

## Internal Supply chain integrity
- Implementation
  - Collect and certify asymmetric public keys from all engineers
  - Have all engineers locally sign all code commits and reviews
  - Multiple independently managed CI/CD systems are deployed
  - CI/CD systems deterministically build only validly signed commits/reviews
  - CI/CD systems sign resulting artifacts with well known/pinned keys
  - Production systems only deploy artifacts signed by multiple CI/CD systems
- Protections
  - Compromised insider impersonates commit as another engineer
  - Compromised insider injects malicious code, bypassing review controls
  - Compromised CI/CD system tampers with artifact generation 
- Resources:
  - https://github.com/distrust-foundation/sig
  - https://github.com/hashbang/git-signatures
  - https://github.com/hashbang/book/blob/master/content/docs/security/Commit_Signing.md
  - https://blog.dbrgn.ch/2021/11/16/git-ssh-signatures/

## External Supply chain integrity
- Implementation
  - Collect and pin asymmetric pubic keys from all code reviewers
  - Review all third party dependencies used in transfer-critical codebases
  - Have all reviewers sign reviews with certified public keys
  - Publish reviews in well documented format to help crowd-source efforts
  - Have CI/CD fail production builds when un-reviewed deps are present
- Protections
  - Obvious malicious code injected into external software library-
- Resources:
  - https://gist.github.com/lrvick/d4b87c600cc074dfcd00a01ee6275420
  - https://gitlab.com/wiktor/lance-verifier
  - https://github.com/in-toto/attestation/issues/77

## Accountable Airgapped Workflows
- Implementation
  - Multiple parties compile deterministic airgap OS and firmware
  - Multiple parties sign airgap os/firmware artifacts
  - New laptop acquired by multiple parties
  - Trusted firmware loaded, verifying signed hash with existing firmware
  - CA key pinned into firmware, and external TPM verification device
  - Laptop stored in highly tamper evident vault requiring multiple parties for access
  - Laptop  firmware verifies multi-party signature on flash-drive iso and any scripts
  - Participants verify date and ensure it is the latest and expected version
- Protections
 - Tampering by any single compromised insider
 - Tampering by any single compiler or build system
- Resources:
  - https://github.com/distrust-foundation/airgap
  - https://github.com/hashbang/airgap 