PORT := 8080
.PHONY: default
default: build-qvs

out:
	mkdir -p out

.PHONY: build-qvs
build-qvs: out/qvs/index.json
out/qvs/index.json: out Containerfile.qvs $(shell find quorum-vault-system -type f)
	mkdir -p out/qvs
	docker \
		build \
		-f Containerfile.qvs \
		--output type=oci,rewrite-timestamp=true,force-compression=true,name=git.distrust.co/public/docs-qvs,tar=true,dest=- \
		. \
	| tar -C out/qvs -mx

.PHONY: serve-qvs
serve-qvs: build-qvs
	tar -C out/qvs -cf - . | docker load
	docker run -p $(PORT):8080 git.distrust.co/public/docs-qvs


