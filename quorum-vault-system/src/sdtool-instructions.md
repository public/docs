/* ANCHOR: all */
# sdtool Usage Guide

[`sdtool`](https://github.com/BertoldVdb/sdtool) is a tool for locking the contents of a SD card by means of burning a physical fuse.

> It is relatively unknown that SD/MMC cards also have an electronic write protection system. Every card has two programmable flags, one for temporary write protection and one to lock the card forever. Writing these flags is not supported by most SD host devices. To remedy this, this page presents a program that allows a Linux host to configure the protection register.

This tool is also available via [stagex](https://registry.hub.docker.com/r/stagex/sdtool). The binary can be exported from the image by doing the following:

// ANCHOR: steps
1. Get deterministically built binary of `sdtool` from StageX: 
    * `docker pull stagex/sdtool`

1. Extracting binary:
    * Run docker container: `docker create -p 4000:80 --name sdtool stagex/sdtool`
      * Copy image to tar: `docker export <container_id> -o sdtool.tar`
      * Extract binary from tar: `mkdir -p sdtool-dir | tar -xvf sdtool.tar -C sdtool-dir | cp sdtool-dir/usr/bin/sdtool ./sdtool`
      * You can verify the container hash:
          * To get container hash: `docker inspect --format='{{json .RepoDigests}}'  stagex/sdtool`
          * Check the [signatures dir](https://codeberg.org/stagex/stagex/src/branch/main/signatures/stagex) in stagex project for latest signed hashes

1. {{ #include finding-device-name.md:content }}

    * Note: the device will not mount as a proper block device on QubesOS so a different OS has to be used where the device appears as /dev/mmcblk<num>

1. `./sdtool /dev/<device_name> permlock`

1. Test that the card can't be written to:

    * Create a test file: 
        
        * `echo "test" > test.txt`

    * Try writing the file to the SD card
        
        * `dd if=./test.txt of=/dev/<device_name> bs=1M conv=sync status=progress`

// ANCHOR_END: steps
/* ANCHOR_END: all */