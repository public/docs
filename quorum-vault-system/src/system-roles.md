# System Roles

There are several roles which are required to properly operate the QVS system. While it is possible to have an individual perform multiple roles, typically they should only perform one role at a time. It is also recommended to have at least 2 individuals, or ideally the full quorum be used to make decisions pertaining to QVS. At least 2 individuals are required for [level 2](threat-model.md#adversary-1).

To better understand why the different roles are required, refer to the [selecting a quorum](selecting-quorum.md) and [threat model](threat-model.md) sections which enumerate a number of assumptions around pertinent threats to the system as well as the use of a quorum.

## General Requirements

Individuals who are selected for the roles:

* MUST have background checks conducted

* MUST have a clearly defined set of responsibilities

* MUST be reinvestigated once a year to ensure they meet necessary standards to access restricted information

## Procurer

Responsible for tasks such as procuring a location, tamper proofing equipment, hardware, and maintaining inventory.

## Provisioner

Responsible for more technical aspects of preparing equipment for ceremonies such as creating air-gapped machines by removing radio cards, and tamper proofing them along with SD cards which are loaded with AirgapOS etc.

## Proposer

This is an individual who is a business owner or stakeholder, or a financial controller. Their role is to make fiduciary decisions which protect the financial interest of the organization and its clients. Their role is specifically to propose the movement of funds, specifying the amount, origin and destination.

## Approver

This is an administrative role which participates in the decision making capacity, typically as part of a quorum. Additional policies which are not for the QVS system but related decision making may be under the purview of an Approver. While there is 1 proposer per transaction, there may be an arbitrary number of Approvers, and they are required to sign proposed transactions according to a [policy](todo) which should be well defined.

## Operator

Trained on how the QVS system operates, with intimate knowledge of the processes which are required to maintain the integrity, confidentiality and availability (CIA triad) of the system.

Operators conduct ceremonies and ensure that the controls around QVS are in tact. They verify instructions from [Approvers](#approver) and perform different actions which are part of the QVS system, ranging across hardware procurement, accessing SCIFs, preparing field kits, performing ceremonies and more.

As a QVS grows, it may be prudent to create more highly specialized roles whose responsibilities are limited to a more narrow range, creating more isolation across the system, thus enforcing the principle of least privilege and separation of concerns.

## Witness

QVS relies of having individuals present to witness that processes which uphold the security of the system are properly followed. [Operators](#operator) make ideal witnesses as their familiarity with the QVS system allows them to detect any deviation from the security-critical processes. While it is not required that a Witness be a trained Operator, it is highly preferred.
