# One Time Use Laptop Ceremony

## Threat Model

One time use laptops are specially prepared for using in field operation but can also be used inside of a secure facility. The primary objective of this setup is that the laptop is provisioned ahead of time, and is considered to be secure for use, but is to be destroyed afterwards.

This flow is the same as [portable reusable laptop ceremony](portable-reusable-laptop-ceremony.md) except instead of tamper proofing the hardware at the end of the ceremony, it is destroyed.