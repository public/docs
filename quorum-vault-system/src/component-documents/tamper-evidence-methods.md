/* ANCHOR: all */

// ANCHOR: entire-doc
# Tamper Evidence Methods

There are different methods which can be used to ensure that objects have not been tampered between uses. This is especially relevant for equipment such as laptops. Each method comes with tradeoffs, and in the context of high assurance security it is instrumental to understand the tradeoffs in order to achieve an adequate level of confidence that supplies such as computers used for high risk operations retain their integrity.

There are a number of common methods which appear to provide a reasonable level of tamper evidence, but in fact do not. It is worth noting a few examples of these such as using tamper evident tape, or even glitter if done improperly. This document will focus on illustrating adequate methods, rather than enumerating ones that are inadequate.

## Properties

Tamper evident methods need to be:

* Difficult to circumvent

* Simple to set up

* Simple to verify

There are three reasonably secure methods which have been identified and are explored in this document that can be used in different contexts:

* Vacuum sealing objects surrounded by colored filler

* Glitter on screws

* Heads / Pureboot for secure boot


#### Level 1 + 2

This threat level assumes fairly unsophisticated attackers, and as such, basic tamper proofing methods can be effective. These attackers would have a difficult time pursuing physical attacks such as evil maiden attacks, or covertly stealing and replacing hardware.

As such one of the following combinations of tamper proofing methods MUST be used:

* [Glitter on screw](#glitter-on-screws) + [pureboot/heads](#pureboot--heads)

* [Vacuum sealing with filler](#vacuum-sealed-bags-with-filler)

#### Level 3

This level of threat actors has a more extensive range of attacks which may include physical attacks. As such additional counter measures are required to ensure that the integrity and confidentiality of information is retained. The threat modelling document contains more information about this [level](threat-model.md#level-3)

* MUST combine [glitter on screws](#glitter-on-screws), [pureboot/heads](#pureboot--heads), and [vacuum sealing with filler](#vacuum-sealed-bags-with-filler)

* MUST maintain 2 person [chain of custody](./hardware-procurement-and-chain-of-custody.md)

#### Level 4

This is the highest threat level and as such requires additional controls which protect hardware. More details around the capabilities of threat actors at this level are available in the [threat modeling document](threat-model.md#level-4)

* MUST use high grade tamper evident safes

* MUST use physical access controls

* MUST have continued surveillance of the storage location

## Vacuum Sealed Bags With Filler
// ANCHOR: vsbwf-whole

One of the most reliable methods for ensuring tamper evidence relies on the randomness and difficulty of placing small objects henceforth referred to as "filler" (colored rice, lentils, confetti) in a transparent bag to encase an object which is then vacuum sealed. By placing an object in a transparent, vacuum sealable bag and surrounding it with filler, an arrangement of the filler around the object in the bag can be achieved which is difficult to reproduce. Upon sealing the object in this manner, photos can be taken to use as a reference once the object is accessed again - allowing one to verify that the arrangement of the filler has not changed.

### Threat Model

There are no known attacks for this type of tamper proofing method when executed properly. The main sources of risk stem from consistent and repeatable photography and comparison of photographs to ensure that any changes can be detected.

If photographs are not cryptographically signed, they can also be manipulated and/or replaced which could result in the compromise of the system as well.

The reason this method is effective is because unlike with many other methods that tamper proof a specific part of an object, such as applying glitter to screws which leaves device ports exposed, or using cryptographic signing to verify the hardware has not been modified, still leaving the door to physical modifications, vacuum sealing with colored filler encases the entire object in a tamper evident manner.

### Adequate Filler

To achieve the best level of randomness and difficulty of reproducing the arrangement of filler in a vacuum sealed bag, a variety of beads of different sizes and color should be used. They may be made of different materials as well but plastic is excellent because it doesn't change form when vacuum sealed - which can make it easier to reproduce patterns. Materials such as confetti and packing beans may be used, but because they can be flattened and retain the shape, arranging them in a given pattern is much easier. Other options like beans or lentils have less variety in color and shapes which makes it harder to detect differences.

Examples of filler:
// ANCHOR:vsbwf-filler
* [B100B5LB – 5 Lb Mixed Craft Bead Bonanza Case](https://www.thebeadery.com/product/b100b5lb-5-lb-mixed-craft-bead-bonanza-case/)
* [Plastic Beads - Multi Color & Size - 700ml](https://www.stockade.ca/Plastic-Beads--Multi-Colour-Size--700ml_p_8402.html)
// ANCHOR_END:vsbwf-filler

### Vacuum Sealers

Vacuum sealer needs to be able to seal bags of sufficient size to fit a 13" laptop

* [Nesco Deluxe Vacuum Sealer – VS-12P](https://www.nesco.com/product/deluxe-vacuum-sealer/)

* [Anova Precision Vacuum Sealer Pro](https://anovaculinary.com/en-ca/products/anova-precision-vacuum-sealer-pro)


Sealing bags of standard size objects which need to be protected can fit in. The bags should be perfectly see through, rather than with writing or any irregularities in the plastic which can obfuscate the view of the inside of the bag. 11" width is recommended.

* [Anova Precision Vacuum Sealer Rolls (11" x 19.60')](https://anovaculinary.com/en-ca/products/anova-precision-vacuum-sealer-rolls)

* [2 Vacuum Sealer Rolls (11.0" x 19.70')](https://www.nesco.com/product/2-vacuum-sealer-rolls-11-0-x-19-70/)

### Additional Considerations

* This strategy may be layered, for example if one chooses to apply it to a hardware token, the sealed hardware token can be placed inside of a bigger bag, along with a laptop.

* A similar method can be used but with a bin filled with filler that the object is placed into. The main disadvantage here is that this type of tamper proofing is not resistant to seismic activity, air movement, or other sourced of vibration which could shift filler around.

### Procedure
// ANCHOR: vsbwf-procedure

#### Requirements
// ANCHOR: vsbwf-equipment
* [Vacuum sealer](tamper-evidence-methods.md#vacuum-sealers)

* [Vacuum plastic roll](tamper-evidence-methods.md#vacuum-sealers)

{{ #include tamper-evidence-methods.md:vsbwf-filler }}

// ANCHOR_END: vsbwf-equipment

#### Sealing
// ANCHOR: vsbwf-procedure-sealing

1. Insert object(s) into plastic sealing bag

1. Fill bag with enough plastic beads that most of the object is surrounded

1. Use vacuum sealer to remove air from the bag until the beads are no longer able to move

1. Take photographs of both sides of the sealed object using both the digital and polaroid camera

1. Date and sign the polaroid photographs and store them in a local lock box

1. Take the SD card to an online connected device, ensuring continued dual custody, and commit the tamper evidence photographs to a repository. If two individuals are present, have one create a PR with a signed commit, and the other do a signed merge commit.

// ANCHOR_END: vsbwf-procedure-sealing

#### Unsealing
// ANCHOR: vsbwf-procedure-unsealing

    a. Retrieve digital/physical photographs of both sides of sealed bundle

    b. Compare all photographs to object for differences

    c. Proceed with unsealing the object if no differences are detected

// ANCHOR_END: vsbwf-procedure-unsealing

// ANCHOR_END: vsbwf-procedure
// ANCHOR_END: vsbwf-whole

## Glitter on Screws

Glitter can be used as an additional control to provide tamper evidence on specific parts of hardware such as laptop screws - in case an adversary attempts to open the laptop and introduce a malicious chip, antenna or otherwise.  While glitter allows to detect physical tampering of the hardware, it does not provide tamper evidence of the firmware and software that runs on the computer, and as such is not sufficient for adequate tamper proofing of laptops on its own.

### Procedure

#### Requirements

* 2 or 3 different types of glitter, ideally with small and large pieces of glitter of different colors

#### Sealing

1. Clean the surface the glitter will be applied to

1. Apply a thin layer of the first type of glitter

1. Wait for it to dry

1. Repeat steps 2, 3 with the different types of glitter being used

1. Take a photograph of the laptop, preferably using the [tamper proofing station](tamper-evidence-methods#tamper-proofing-station)

1. Ensure the SD card is in dual custody until its contents are uploaded to a repository, and signed by both parties (one creates a PR, the other creates a signed merge using the `git` CLI)

#### Verification

There is no "unsealing" procedure as the glitter used on screws, or in other similar contexts is meant as a more permanent control. As such the primary action that's performed is the verification of the integrity of the tamper proofing seal.

To verify that the seal has not been tampered, compare the glitter arrangement to a photograph which had been previously signed and stored. Both operators should have a copy of the picture and use it to verify the integrity of the seal.

## PureBoot / Heads

This tamper proofing method is designed to protect the secure boot process of a computer. It does not protect the computer from physical tampering which can be used to ad

### Procedure

Refer to the [PureBoot Setup](./enable-pure-boot-restricted-boot.md) document

## Tamper Proofing Station

The Tamper Proofing Station is a simple structure used to make it easy to take photographs which have consistent lightning, consistent angle, and consistent distance from the object being photograph. In this manner, photographs can be taken which ensure that any differences in the sealed object can be easily detected.

### Instructions

To construct an appropriate Tamper Proofing Station, the simplest setup consists of:

* Overhead camera mounting rig

* Powerful LED light which can be attached to the mounting rig

## Polaroid camera

* Can be attached to mounting rig

### Models
// ANCHOR: polaroid-cameras
* [Polaroid Now+](https://www.amazon.com/Polaroid-Generation-Bluetooth-Connected-Controlled/dp/B0BVNJHMVQ)

* Preferred film: [Color I-Type Film](https://www.amazon.com/Polaroid-Originals-Instant-Color-I-Type/dp/B084GXXLM7)
// ANCHOR_END: polaroid-cameras

Pick a location for the station, and attach the LED light and the camera to the overhead camera mounting rig. Set up the camera so that when it's turned on, a 14" laptop is perfectly framed without having to zoom in or out if possible.

## High Visibility Storage

The purpose of high visibility storage is to provide a way to keep items which are used during a ceremony from risk of being swapped by one of the participants in the ceremony. As such, a high visibility storage should be a plastic container which is sealed, and which is only opened under the close supervision of a quorum of individuals.

Some examples include:

* Large glass jar

* Plastic bag

## Safe

Placing objects into a safe helps improve the security of objects, and introduces an additional layer of tamper evidence.

## References

* [Blog About Tamper Evident Protection Methods](http://web.archive.org/web/20241130002204/https://dys2p.com/en/2021-12-tamper-evident-protection.html)

* [BitBoxTep - tamper-evident packaging](http://web.archive.org/web/20240519013739/https://medium.com/shiftcrypto/an-introduction-to-bitboxtep-our-new-tamper-evident-packaging-8fa06d983c32)

* [Mullvad - glitter tamper proofing](http://web.archive.org/web/20240317004315/https://mullvad.net/en/blog/how-tamper-protect-laptop-nail-polish)

* [Purism anti-interdiction](http://web.archive.org/web/20241121233006/https://puri.sm/posts/anti-interdiction-services/)

* [Purism Liberty phone anti-interdiction](http://web.archive.org/web/20240903104700/https://puri.sm/posts/anti-interdiction-on-the-librem-5-usa/)
// ANCHOR_END: entire-doc

/* ANCHOR_END: all */
