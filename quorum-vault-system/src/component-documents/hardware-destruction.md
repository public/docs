# Destroying Hardware

Destroying hardware should be done by using a combination of:

* Degaussing

* Burning

* Shredding

* Pulverizing

All three methods should be used because of the efficacy of using electron
microscopy to read data from storage drives which have not been completely
destroyed.

Drilling through the storage drive, a common hardware destruction method, is not
considered to be secure. In fact it's best to remove drive from the device and
only use in memory storage.

Ensuring that all chips are completely physically destroyed is essential.

In the best case scenario, the hardware should be melted in a foundry, as this
makes it impossible to retrieve any data by any means.