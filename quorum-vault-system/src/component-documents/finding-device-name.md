/* ANCHOR: all */
// ANCHOR: content
Look for your SD card device name (`<device_name>`) in the output of the `lsblk` command. It will typically be listed as `/dev/sdX` or `/dev/mmcblk<num>`, where X is a letter (e.g., `/dev/sdb`, `/dev/sdc`). You can identify it by its size or by checking if it has a partition (like `/dev/sdX1`)
    * Mount the device using: `sudo mount /dev/<your_device> /media`
// ANCHOR_END: content
/* ANCHOR_END: all */
