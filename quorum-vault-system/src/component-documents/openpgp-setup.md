/* ANCHOR: all */
# OpenPGP Setup

## Generating Keys using `keyfork`
// ANCHOR: steps-keyfork

1. Insert an SD card into the system

1. Change working directory to SD card mount location
    ```
    $ cd /media/TRANSFER
    ```

1. Insert all smartcards to be provisioned into the system.

1. Set expiry time via environment variable - you can update 2y to desired value:

    ```
    $ export KEYFORK_OPENPGP_EXPIRE=2y
    ```

1. Generate a mnemonic, encrypting to a newly-generated key:

	Ensure the User ID is your name and your email.

    ```
	$ keyfork mnemonic generate --encrypt-to-self encrypted.asc --provision openpgp-card --derive='openpgp --public "Your Name <your@email.co>"'
    ```

	The `--provision-count` option can be provided to ensure the correct amount
	of smartcards is provisioned - the program will error if the amount of
	smartcards available is not equal to the count requested.

	Note: The PIN can't use sequential numbers, characters or repeated patterns.

// ANCHOR_END: steps-keyfork

## Generating Keys on Smartcard
// ANCHOR: steps-on-key-gen

1. Insert the smart card into the USB port if it is not already plugged in.

1. Open Command Prompt (Windows) or Terminal (macOS / Linux).

1. Enter the GPG command:

	* `gpg --card-edit`

1. At the gpg/card> prompt, enter the command: admin

1. If you want to use keys larger than 2048 bits, run: key-attr

1. Enter the command: generate

1. When prompted, specify if you want to make an off-card backup of your encryption key.

    * Note: This is a shim backup of the private key, not a full backup, and cannot be used to restore the key.

1. Specify how long the key should be valid for (specify the number in days, weeks, months, or years).

1. Confirm the expiration day.

1. When prompted, enter your name.

1. Enter your email address.

1. If needed, enter a comment.

1. Review the name and email, and accept or make changes.

1. Enter the default admin PIN again. The green light on the smart card will flash while the keys are being written.

1. Enter a Passphrase as the key will not allow you to pass without having a passphrase. If you do not enter a Passphrase generation will fail.

// ANCHOR_END: steps-on-key-gen

/* ANCHOR_END: all */
