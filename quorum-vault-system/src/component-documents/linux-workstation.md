/* ANCHOR: all */
# Linux Workstation (Online Machine)
// ANCHOR: content
* Linux Workstation (online machine)
  * Any internet connected computer with a Linux shell will suffice
// ANCHOR_END: content
/* ANCHOR_END: all */