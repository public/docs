# `autorun.sh` Setup

This document describes how `autorun.sh`, a file that AirgapOS automatically boots and runs should be set up.

This setup can be done on any machine.

1. Create a file called `autorun.sh` 

    * In your Terminal use this command: `vi autorun.sh`

    * Once you are in the editor press "i" to enter "insert mode"

    * Type in the contents, replacing <M>, <N>, <number_of_smart_cards_per_operator>, <pgp_user_id> with your chosen threshold numbers according to your [Quorum](selecting-quorum.md):

    ```sh
    #!/bin/sh
    keyfork mnemonic generate --shard-to-self shardfile.asc,threshold=<M>,max=<N>,cards_per_shard=<number_of_smart_cards_per_operator>,cert_output=keyring.asc --derive='openpgp --public "<pgp_user_id>" --output certificate.asc'
    ```

    * Press "esc"
    * Press ":"
    * Press "x"
    * Press Enter

1. Hash the file
    The file should be hashed by using the following command:

    ```sh
    sha256sum autorun.sh
    ```
    Make note of the hash on a piece of paper

1. Copy the file to the Storage Device which contains AirgapOS.

    a. If you don't have a Storage Device set up with AirgapOS use [this guide](repeat-use-airgapos.md) to do so.

    b. Mount the AirgapOS Storage Device using [this guide](storage-device-management.md#mounting-a-storage-device)

    c. Copy the `autorun.sh` file to the Storage Device

1. Make note of this hash on a piece of paper or print it as you will need it to verify the file during Ceremonies.
