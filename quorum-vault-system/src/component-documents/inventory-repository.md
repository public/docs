
/* ANCHOR: all */
# Inventory Repository
// ANCHOR: content

This repository is used to keep track of available inventory and tamper proofing evidence

## Repository Structure

```
computers/
    <num>/
        description.txt
        tamper_evidence_front.jpeg
        tamper_evidence_back.jpeg
bundles/
    <num>/
        description.txt
        tamper_evidence_front.jpeg
        tamper_evidence_back.jpeg
sd_cards/
    <num>
        ...
```

## Procedure: Setting up Repository

{{ #include ./git-repository-initialization.md:procedure}}

// ANCHOR_END: content
/* ANCHOR_END: all */