/* ANCHOR: all */
# PureBoot Hash Verifying .iso Setup

If the SD card with AirgapOS is stored as part of a Air-Gapped bundle, then doing this secure boot sequence is only necessary the first time. Of course, it doesn't hurt to use this method as an additional precaution, reducing the risk that one of the operators can swap out the SD card for a different one during a ceremony.

This section can be completed on any machine.

AirgapOS has `keyfork` and `icepick` built into it for cryptographic operations such as key derivation and signing

// ANCHOR: steps
1. Build the software according to the [readme](https://git.distrust.co/public/airgap) in the repository.Use the `make reproduce` command.

2. Verify the software according to [this guide](./component-documents/verifying-signatures.md)

3. Place signed .iso on a storage device

    a. Import the public key for the PureBoot Smart Card from [Initialize PureBoot Smart Card](initialize-pureboot-smart-card.md) guide.
    ```
    gpg --import <ID>.asc
    ```

    b. Get the GPG key ID using the command:
    ```
    gpg --list-keys
    ```
    It should look something like `6B61ECD76088748C70590D55E90A401336C8AAA9`

    c. Sign the `airgap.iso` image using the "PureBoot Smart Card", which is
    set up in [Initalize PureBoot Smart Card](initialize-pureboot-smart-card.md)
    guide.
    ```
    gpg --detach-sign --armor --default-key <ID> airgap.iso
    ```

4. Copy `airgap.iso` and detached signature to a storage device

    a. Select a new Storage Device which has no other files on it and plug it
    into the machine that has the `airgap.iso` file and the detached GPG signature.

    b. Find the name of the Storage Device using [this guide](storage-device-management.md#finding-a-storage-device-name)

    d. Use the `mount` command to mount the drive

    e. Copy both the `airgap.iso` and detached signature to the drive.

5. Lock the SD card using `sdtool`

6. Make sure to note the `sha256sum` hash of the `airgap.iso` and write it
down on a piece of paper.

7. Multiple members of your team should build the `airgap.iso` image
independently and use `sha256sum airgap.iso` in order to hash it, then record
the value for later use. This value will be checked during Ceremonies before
booting the ISO image to ensure it can be trusted.

// ANCHOR_END: steps
/* ANCHOR_END: all */