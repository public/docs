/* ANCHOR: all */
// ANCHOR: content
1. Connect SD card to online linux workstation

1. {{ #include finding-device-name.md:content }}

1. If the `~/vaults/` repository already exists, ensure it doesn't have any changes that haven't been committed, then remove it using `sudo rm -rf ~/vaults` before re-running the previous step

1. Copy the repository with updated files to an online linux workstation, sign, commit and push to the `vaults` repository:
    ```
    $ cp -r /media/vaults ~/vaults/
    $ cd ~/vaults
    $ git add .
    $ git commit -m -S "<message>"
    $ git push origin HEAD
    ```
// ANCHOR_END: content
/* ANCHOR_END: all */
