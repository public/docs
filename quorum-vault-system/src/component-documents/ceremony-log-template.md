## Ceremony Log Template

```yml
usage: Location Key
officiant: Anton Livaja
location: Private Home (Address Redacted)
witnesses: N/A
hardware: Dell XPS 13 9630
firmware: BIOS 2.13.0
laptop_modifications:
  - Removed WLAN Card
  - Removed speakers
  - Removed microphone
  - Removed all drives
boot_media: Kingston Type 2 SD Card 1GB
backup_media: TeamGroup High Endurance Micro SDXC 128GB
smart_cards: Yubikey 5 NFC
software:
  - name: Airgap OS
    repo: https://git.distrust.co/public/airgap
    ref: main
    hash: 485fc58bfb1b4dc75a81138d93948385cc5bf600
playbooks:
  - name: some/path/to/location_key_generation.md
    repo: https://git.distrust.co/public/docs
    ref: some-git-ref-here
    notes: used once for each Location Key
  - name: some/path/to/hybrid_quroum_key_generation.md
    repo: https://git.distrust.co/public/docs
    ref: some-git-ref-here
    notes: used once to generate Root Entropy and Disaster Recovery Key
outputs:
  - cert: ./cert
  - shardfile: ./shardfile
Location (Test) Public Key Fingerprints:
  - 0609D5C2634DB5D75226AD9A7A8A6F24873977E4
  - 5F827701822425E8BB0D2EAB43EC881D8C80DE41
  - 6E18E082945BC43411C3B490E43B49017440605D
Cold Quorum Key (Test) Fingerprint:
  - 8BA0304345D05775C303E292D9BDBC00D3E85E87
log:
  - 2024-08-05:1723: Selected a room in residence which has no electronics in it
  and closed window and window blinds.
general_notes: N/A
```