/* ANCHOR: all */
# Hardware Models

## Computers 

* Laptops with chargers over ports which don't allow data transfer is preferred (non USB etc.)

// ANCHOR: computer-models 

* HP 13" Intel Celeron - 4GB Memory - 64GB eMMC, HP 14-dq0052dx, SKU: 6499749, UPC: 196548430192, DCS: 6.768.5321, ~USD $179.99
    * [Illustrated Parts Catalog](https://h10032.www1.hp.com/ctg/Manual/c04501162.pdf#%5B%7B%22num%22%3A3160%2C%22gen%22%3A0%7D%2C%7B%22name%22%3A%22XYZ%22%7D%2Cnull%2C732%2Cnull%5D)

* Lenovo 14" Flex 5i FHD Touchscreen 2-in-1 Laptop - Intel Core i3-1215U - 8GB Memory - Intel UHD Graphics, SKU: 6571565, ~USD $379.99

* Purism Librem 14
// ANCHOR_END: computer-models

## SD Cards
// ANCHOR: sd-models

* [Kingston Industrial 8GB SD Memory Card](https://www.kingston.com/en/memory-cards/industrial-grade-sd-uhs-i-u3?capacity=8gb)

* [Kingston Indsutrial 8GB microSD Memory Card](https://shop.kingston.com/products/industrial-microsd-card-memory-card?variant=40558543405248)

* microSD to SD adapter

    * [64GB Kingston Canvas Select Plus Class 10 MicroSDXC Memory Card with SD Adapter (SDCS2/64GB)](https://bulkmemorycards.com/shop/microsd-cards/microsd-64gb/microsd-64gb-class-10/microsd-64gb-class-10-w-sd-adapter/64gb-kingston-canvas-select-class-10-microsdxc-memory-card-with-sd-adapter-sdcs-64gb/?_gl=1*1r3cz3m*_up*MQ..*_gs*MQ..&gclid=Cj0KCQiAvvO7BhC-ARIsAGFyToVLF285A59zXpHQEDA0sc7NML5JQohdIOPnS1o-6IfjqClWWZdMruUaAupkEALw_wcB) 

* SD Card USB Adapters

    * SD card reader: https://www.kingston.com/en/memory-card-readers/mobilelite-plus-sd-reader

    * microSD card reader: https://www.kingston.com/en/memory-card-readers/mobilelite-plus-microsd-reader

    * Workflow station hub (may prove helpful with workflows): https://www.kingston.com/en/memory-card-readers/workflow-station-hub

// ANCHOR_END: sd-models

## Smart Cards

Smart Cards are primarily used for storing OpenPGP cryptographic keys which are
used as a building block for security controls. These smart cards hold OpenPGP
keys which are derived in secure environments.

There are three primary requirements for smart cards:

* FIPS 140-2

* Support for Ed25519 OpenPGP

* Touch for enacting operations

### Notes

* Librem smartcards are not recommended because they don't have touch capabilities

* NitroKey and SoloKey are favored due to their fully open nature and therefore verifiability

* YubiKey has the advantage of being the most battle tested but is not verifiable and has had issues in the past (Infineon bug)

Some options include:
// ANCHOR: smart-cards 

* NitroKey 3 

* YubiKey 5

// ANCHOR_END: smart-cards 

## Tamper Proofing 

// ANCHOR: sealable-plastic-bags
[Alert Security bag](https://shop.alertsecurityproducts.com/clear-alert-bank-deposit-bag-15-x-20-250cs?affiliate=ppc12&gad_source=1&gclid=CjwKCAiAgoq7BhBxEiwAVcW0LJoCVUqYI1s4RGoctHxMwtmNlwenDhgP_0x4gjB9W2e4f_7tzdJ_rxoCOwMQAvD_BwE)
// ANCHOR_END: sealable-plastic-bags

/* ANCHOR_END: all */