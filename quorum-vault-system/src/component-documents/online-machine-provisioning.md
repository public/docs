# Online Machine Provisioning

## QubesOS

QubesOS is a preferred operating system for use in high security assurance scenarios as it uses hardware based virtualization leveraging the Xen hypervisor, which gives strong isolation guarantees. This makes it trivial to create purpose specific environments, which have minimal software footprints, as well as restricted networking in order to limit ingress and egress.

* [Hardware Compability](https://www.qubes-os.org/hcl/)

    * It is highly preferred to use a Purism machine due to additional hardware supply chain security features such as anti-interdiction

    * Commonly used alternative makes include: ThinkPads, Framework and Dell

* [Installation](https://www.qubes-os.org/downloads/)

    * MUST follow "verifying signatures" guide

## "Power-Washed" Chromebook with ChromeOS

In order to reduce surface area for attacks, we can reset a Chromebook to its factory settings, effectively wiping any malicious software that may have made its way onto the system during previous use.

### "Power-Washing"

1. Press and hold the Ctrl + Alt + Shift + R keys on your keyboard.

2. Select the Restart option.

3. A screen will appear asking you to confirm that you want to reset the device. Click Powerwash and Reset, then Continue.


