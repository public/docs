# Storage Device Management

In order to interact with a USB device on a Linux system it first has to be
`mounted`. In order to mount a USB device, one must first identify the device
name, and then use the `mount` command.

## Finding a Storage Device Name
USB devices are assigned names when they are connected to a Linux operating
system. The first storage device is assigned the name `sda` (storage device a),
the second `sdb`, the third `sdc` and so on.

One may use the `lsblk` to list the detected storage devices for a system, which will output something like this:
```
NAME    MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
xvda    202:0    1    50G  0 disk
├─xvda1 202:1    1   200M  0 part
├─xvda2 202:2    1     2M  0 part
└─xvda3 202:3    1  49.8G  0 part /
xvdb    202:16   1   1.9T  0 disk /rw
xvdc    202:32   1    10G  0 disk
├─xvdc1 202:33   1     1G  0 part [SWAP]
└─xvdc3 202:35   1     9G  0 part
xvdd    202:48   1 526.8M  1 disk
```

Then after plugging in the Storage Device run `lsblk` and look for what's
different. In this example, the newly detected storage device is `sdb`:
```
NAME    MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sdb       8:16   1  14.5G  0 disk
xvda    202:0    1    50G  0 disk
├─xvda1 202:1    1   200M  0 part
├─xvda2 202:2    1     2M  0 part
└─xvda3 202:3    1  49.8G  0 part /
xvdb    202:16   1   1.9T  0 disk /rw
xvdc    202:32   1    10G  0 disk
├─xvdc1 202:33   1     1G  0 part [SWAP]
└─xvdc3 202:35   1     9G  0 part
xvdd    202:48   1 526.8M  1 disk
```

## Mounting a Storage Device
In order to mount a device, first ensure that the directory you will mount to
exists by running:

```sh
mkdir -p /media/usb
```

Then run the following command to mount the storage device:
```sh
sudo mount /dev/<usb_device_name> /media/usb