# PureBoot Setup

- [ ] TODO: fix this doc to use a different smart card for pureboot as the librem key, as the librem key doesn't have a physical switch

- [ ] TODO update this to be hardware agnostic and use Heads / PureBoot

This guide walks the user through setting up a machine which relies on
[PureBoot](https://source.puri.sm/firmware/pureboot) to verify the authenticity
of the `.iso` image which is being booted, as well to ensure that firmware of
the machine has not been tampered between uses.

This guide assumes the use of a Purism machine, with a Librem Key.

## Requirements

* 1 Storage Device

* 1 Smart Card

* 1 Librem 14 Computer with [PureBoot firmware installed](flash-pureboot-firmware.md).

## Notes

After you complete this setup, the Librem Smart Card will be provisioned with a
new GPG key pair, which will be used for signing the BIOS, as well as any `.iso`
images which will be booted using the [Restricted Boot](https://docs.puri.sm/PureBoot/Restricted.html)
mode.

At the end of this guide you will have:

* 1 Librem Smart Card

    * With a newly generated GPG key pair

    * With a newly generated HOTP secret

* 1 storage device with the public key of the newly generated GPG key

    * This GPG key will be used to sign `.iso` files booted on the machine

## Steps

1. Plug in the Librem Smart Card into the machine

2. Turn on the machine

3. Wait for the prompt that says "Automatic boot in 5 seconds unless interrupted
by keypress..."

    * Press *any* key

4. Select "Options -->"

    * Press Enter

5. Select "GPG Options" -->

    * Press Enter

6. Select "Generate GPG keys manually on a Librem Key"

    * Press Enter

7. Please Confirm that your GPG card is inserted [Y/n/]

    * Input "Y", press Enter

8. $ gpg/card>

    * Input `admin`, press Enter

9. $ gpg/card>

    * Inpuut `generate`, press Enter

10. Make off-card backup of encryption key (Y/n):

    * Input "n", Press Enter

11. Replace existing keys? (y/n):

    * Input "y", press Enter

12. PIN: <user pin> (default is 123456)

    * Input `user_pin`, press Enter

13. Key is valid for? (0):

    * Press Enter

14. Key does not expire at all. Is this correct? (y/N):

    * Input "y", press Enter

15. Real name: <name>

    * Note: You must supply at least one of the "Real name", "Email address"
        or "Comment"
    * Input one of the values, and press Enter

16. Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit?

    * Input "O", press Enter

17. Admin PIN: <admin pin> (default is 12345678)

    * Input `admin_pin`, press Enter

18. After step q, the generation of the key will take some time then you will
see a prompt:
    ```
    gpg: key<ID> market as ultimately trusted
    gpg: directory '//.gnupg/openpgp-revocs.d' created
    gpg: recovation certificate stored as '//.gnupg/openpgp-revocs.d/<ID>.rev'
    public and secret key created and signed
    ```

19. $ gpg/card>

    * Input "quit", press Enter

20. "Would you like to copy the GPG public key you generated to a USB disk?
       You may need it, if you want to use it outside of Heads later.
       The file will show up as <ID>.asc"

    * Ensure a USB drive is connected

    * Select "Yes", press Enter

21. "Would you like to add the GPG public key you generated to the BIOS?
        This makes it a trusted key used to sign files in /boot"

    * Select "Yes", press Enter

22. "Would you like to update the checksum and sign all of the files in /boot?
        You will need your GPG key to continue and this will modify your disk
        Otherwise the system will reboot immediately."

    * Select "Yes", press Enter

23. Please confirm that your GPG card is inserted [Y/n]:

    * Input "Y", press Enter

24. After the computer reboots you will be faced with an error:
    "ERROR: PureBoot couldn't generate the TOTP code."

    * Select "Generate new HOTP/TOTP secret", press Enter

25. "This will erase your old secret and replace it with a new one! Do you want
to proceed?"

    * Select "Yes", press Enter

