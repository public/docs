/* ANCHOR: all */
# Secure Boot Sequence

// ANCHOR: content
Steps 1-12 can be skipped if the media drive with `airgap` has been verified in
advance.

1. Plug in the [PureBoot Smart Card](initialize-pureboot-smart-card.md)

2. Plug in [AirgapOS Storage Device](repeat-use-airgapos.md)

3. Turn on the machine

4. Press space when the message "Automatic boot in 5 seconds unless interrupted by keypress..."

5. Once in the PureBoot Boot Menu, navigate to "Options -->" and press Enter

6. Navigate to "Exit to recovery shell" and press enter

7. Use the command `source /etc/gui_functions` to load gui functions

8. Use the command `mount_usb` to mount the Storage Device which contains `airgap.iso` and the detached GPG signature.

9. Type `sha256sum /media/airgap.iso.asc`

10. Verify the hash that appears using whatever number of witnesses the Quroum
agreed are necessary for witnessing key parts of the Ceremony. Each witness
should bring their own piece of paper with the hash written out based on the
binary they built on their own system according to the [AirgapOS Setup Playbook](repeat-use-airgapos.md).

12. Once everyone is satisfied that the hash matches, the computer should be
be restarted.
// ANCHOR: prepared
13. Press space when the message "Automatic boot in 5 seconds unless interrupted by keypress..."

14. Once in the PureBoot Boot Menu, navigate to "Options -->" and press Enter

15. Navigate to "Boot Options -->" and press enter

16. Navigate to "USB boot" and press enter

17. Ensure that `/media/airgap.iso` is selected and press Enter

18. Once booted, verify the version of the software matches the AirgapOS Hash
which was noted during the [AirgapOS Setup](repeat-use-airgapos.md).
// ANCHOR_END: prepared
// ANCHOR_END: content

/* ANCHOR_END: all */