# Flash Pureboot Firmware

The version required for being able to complete all steps for setting up the
Ceremony Machine using a Librem 14:
* https://source.puri.sm/firmware/releases/-/blob/PureBoot-Release-24-L14JackDetect-1/librem_14/pureboot-librem_14-Release-24-L14JackDetect-1.rom.gz?ref_type=heads

If another type of hardware is used you may have to try different versions of
the PureBoot firmware as some have issues with the `/etc/gui_functions` and `/etc/functions` which are used during the [Secure Boot Sequence](secure-boot-sequence.md)

## Steps

1. Download the [.rom](https://source.puri.sm/firmware/releases/-/blob/PureBoot-Release-24-L14JackDetect-1/librem_14/pureboot-librem_14-Release-24-L14JackDetect-1.rom.gz?ref_type=heads) file

2. Put the `.rom` file on a Storage Device

3. Plug the Storage Device with the `.rom` file into the Librem 14

4. During boot, press *any* key when prompted

5. Select "Options -->", press Enter

6. Select "Flash/Update the BIOS -->", press Enter

7. Select "Flash the firmware with a new ROM, retain settings"

8. "You will need to insert a USB drive containing your BIOS image (*.rom...)
    After you select this file, this program will reflash your BIOS.
    Do you want to proceed?"

    * Select "Yes", Press Enter

9. Select the `.rom` file and press Enter

10. Do you wish to proceed?

    * Select "Yes", Press Enter
