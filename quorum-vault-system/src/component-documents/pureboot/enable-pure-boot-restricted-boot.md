# Enable PureBoot Restricted Boot

With PureBoot Restricted Boot, you can lock down your boot firmware to only boot
trusted, signed executables both on a local disk and USB, so you control the
keys.

In order to enable Restricted Boot, the user may follow the Purism guide found
[here](https://docs.puri.sm/PureBoot/Restricted.html).