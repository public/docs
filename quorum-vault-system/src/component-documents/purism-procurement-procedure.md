# Purism Procurement Procedure (Anti-Interdiction)

1. Select a librem 14 laptop from https://puri.sm, and ensure:
    
    * Memory: 8GB

    * Storage: 250GB

    * Power Adapter Plug: US

    * Wireless: No wireless

    * Firmware: PureBoot Bundle Anti-Interdiction (PureBoot Bundle Plus + Anti-interdiction services)

    * Operating System: PureOS

    * Warranty: 1 Year

    * USB Flash Drive: No USB Flash Drive

2. Purism will reach out via email and establish secure communications using PGP, so ensure that the individual who is in charge of procurement has a PGP key that's been set up securely. Purism will:

    * Modify the laptop as per order specifications, in this case removing radio cards.

    * Seal the screws on the bottom of the laptop using glitter of chosen color

    * Take photographs of the inside of the laptop, then of the outside after it's sealed

    * The photographs will be signed by Purism and encrypted to the PGP key used for communications to protect the integrity of the images

    * The firmware verification hardware token can be sent to a separate location from the laptop, and will be tamper sealed using tamper proofing tape 
        
        * TODO: find out if we can have vacuum sealing with filler as a tamper proofing method be provided by Purism

    * The laptop will be sealed in a box using tamper proofing tape

3. Once the laptop is received, it should not be opened until at least 2 parties are present and principles of [chain of custody](./hardware-procurement-and-chain-of-custody.md) can be upheld. The images of tamper proofing provided by Purism should be used to ensure that the hardware had not been tampered, and the hardware token to verify firmware is in tact.

4. Once the hardware is properly verified to not have been tampered in transit, a [tamper evidence method](../tamper-evidence-methods.md) should be applied to the laptop before it's stored.