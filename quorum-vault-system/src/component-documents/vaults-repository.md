/* ANCHOR: all */
# Vaults Repository

// ANCHOR: content
This repository holds data pertaining to vaults. The primary data consists of:

* Operation proposals

* Operation approvals

* Payloads

* Trusted PGP keyring

* Shardfiles

* Blockchain metadata

* Policies (such as spending rules)

* Ceremony logs 

## Directives

* MUST be a private repository

* MUST require signed commits

## Repository Structure

```
keys/
    all/
        fingerprint.asc
<namespace>/
    ceremonies/
        <date>/
            log.txt
            payloads/
                payload_<num>.json
                payload_<num>.json.sig
    blockchain_metadata/
        sol_nonce_address.txt
    policies/
        spending-policy.json [NOT IMPLEMENTED]
    keyring.asc
    shardfile.asc
```

## Procedure: Setting up Repository

{{ #include ./git-repository-initialization.md:procedure}}

// ANCHOR_END: content
/* ANCHOR_END: all */

