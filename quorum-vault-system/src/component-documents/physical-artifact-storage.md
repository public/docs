# Physical Artifact Storage

QVS requires that some of the hardware containing cryptographic material be
securely stored in physical locations. The two primary cases where physical
storage is necessary are the storage of Location Key Smart Cards, and Operator
Key Smart Cards. These Smart Cards are necessary to successfully execute a
ceremony.

There are two primary physical artifacts which need to be stored properly:

* Operator Smart Cards

* Location Smart Cards

## Operator Smart Cards

These cards should be stored by Operators in personal vaults using a high
quality hidden safe, or in a vaulting facility such as a bank vault, or a
private vaulting provider.

## Location Smart Cards

These cards should only be stored in secure vaults which meet the criteria
outlined for Storage Locations in the [Location](locations.md) document.

## Additional Criteria

* MUST apply glitter nail polish to screws/seams of hardware casing, and take
photographs. It's best to use nail polish where the glitter is of different
sizes and colors.

* MAY put the hardware in a vacuum sealed bag with confetti, and take
photographs.

* MUST place smart cards in a tamper proof bag, whose picture is taken upon
sealing, and stored along with other [Public Ceremony Artifacts](public-ceremony-artifact-storage.md)

* SHOULD provision all GPG keys to at least two smart cards, ideally made
by different manufacturers in order to decrease the likelihood that they both
simultaneously experience a hardware failure.

* SHOULD place the smart cards in anti-static bags

* SHOULD place the smart cards in a faraday bag
