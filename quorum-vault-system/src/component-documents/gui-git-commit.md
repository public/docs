/* ANCHOR: all */
# Committing Using Git Graphical User Interface

The GitKraken tool can be used to produce commits with data.

    - [ ] TODO deterministic build of GitKraken / custom tool for this
    - [ ] TODO maybe it's better to train the team to use `git` in the terminal for now until we have a better tool because GitKraken introduces a lot of surface area for attacks

# GitKraken Guide: Create a File, Edit in VS Code, and Commit
// ANCHOR: steps
1. Clone the Repository
    * Launch the GitKraken application.
    * Clone the ceremony repository:
        * Click on the **"Clone"** button on the left sidebar.
        * Enter the repository URL you want to clone.
        * Choose a local directory where you want to save the repository.
        * Click **"Clone the repo"**.

1. Create a new file
    * **Navigate to the repository**: Make sure you are in the cloned repository in GitKraken.
    * **Create a new file**:
        * Right-click on the folder where you want to create the file in the left sidebar.
        * Select **"New File"**.
        * Name your file (e.g., `<file_name>`).

1. Open the File in Visual Studio Code
    * **Open Visual Studio Code**:
        * Right-click on the newly created file
        * Select **"Open in External Editor"** (this should open the file in Visual Studio Code)

1. Add content to the file
    * In Visual Studio Code, type a simple JSON blob. For example:
     ```json
     {
       "name": "Sample",
       "version": "1.0.0",
       "description": "This is a sample JSON blob."
     }
     ```
    * Save the file: Press `Ctrl + S` (or `Cmd + S` on Mac) to save the changes.

1. Stage the changes
    * **Return to GitKraken**: Go back to GitKraken.
    * **Stage the File**:
        * In the left sidebar, you will see the file you just created under the **"Unstaged Files"** section.
        * Click the checkbox next to `<file_name>` to stage the file.

1. Commit the Changes
    * **Commit the Changes**:
        * In the commit message box at the bottom, type a commit message (e.g., "Add <file_name> with sample JSON blob").
        * Click the **"Commit changes"** button.

1. Push the Changes (if needed)
    * Push to remote repository:
        * If you want to push your changes to the remote repository, click the **"Push"** button in the top toolbar.
// ANCHOR_END: steps

/* ANCHOR_END: all */