/* ANCHOR: all */
# Procurement & Chain of Custody

## Provisioning Chain of Custody

Materials and devices which are used in the context of a high assurance system need to be monitored carefully from the moment they are purchased to ensure there are no single points of failure. Going back to the assumption that participants in the system are subject to [MICE](../glossary.md#MICE) and as such may pose a threat to the system, special care has to be taken that multiple individuals are involved in the whole lifecycle of provisioning a piece of equipment.

All steps of the provisioning process need to be completed under the supervision of at least 2 individuals, but benefit from having even more individuals present to increase the number of witnesses and allow individuals to take washroom breaks, eat etc.

The following steps must all be completed under the continued supervision and with the involvement of all parties present. It is instrumental that there is not a single moment where the device is left unsupervised, or under the supervision of only 1 individual.

## Provisioning Equipment
// ANCHOR: steps

1. Selecting a Purchase Location

    * Select at multiple stores which carry the type of equipment being purchased, then randomly select one using the roll of a die, or other random method. This is done in order to reduce the likelihood that an insider threat is able to plant a compromised computer in a store ahead of time.

1. Within the store, identify available adequate device

1. Purchase the device and place it in a see-through plastic bag which will be used to transport it to a "processing location", which SHOULD be an access controlled space. 
    * The bag MUST be a sealable see-through tamper evident bag. It may be necessary to remove the device from it's original packaging to fit it into the sealable bag.

1. If the equipment does not have to be tamper proofed, simply deliver it to its storage location, and update the inventory repository with the serial number of the device.

1. If the equipment does require tamper proofing, apply the appropriate level of tamper proofing for the security level you are performing the procurement for. 

// ANCHOR_END:steps
/* ANCHOR_END: all */