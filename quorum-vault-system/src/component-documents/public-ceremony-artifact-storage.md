# Redundant Storage of Ceremony Artifacts

Ceremony Artifacts consist of data which is not sensitive in nature, but
essential to ongoing operation of a QVS.

The primary artifacts which are produced during the ceremony are:

* PGP Key Certificates

* Shard Files

* Pin Files

* Pictures of tamper proof measures such as tamper proof bags, glitter applied
to bags, or hardware, such as screws on the bottoms of laptops etc. Pictures
should be cryptographically signed to prevent tampering.

## Key Principles

* MUST verify that all Public Artifacts generated during the Ceremonies are
present in the Ceremony Artifacts bundle.

* MUST store all Ceremony Artifacts in at least 2 different cloud storage
systems. Some options are:

    * AWS S3

    * Google Cloud Platform Storage

    * Azure Storage

    * GitHub

* The data SHOULD have proper access controls, and only be accessible to
authorized personnel who are part of the organization.
