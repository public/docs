/* ANCHOR: all */
# Git Commit Signing
// ANCHOR: steps
1. Retrieve the value of your PGP key ID from smartcard

    ```
    gpg --card-status
    ```

1. Configure git to sign commits with smartcard
    ```
    $ git config --global user.name <name>
    $ git config --global user.email <email>
    $ git config --global user.signingKey <pgp_key_id>
    $ git config --global commit.gpgsign = true
    $ git config --global commit.merge = true
    ```

1. Configure ssh to authenticate with smartcard

    ```
    $ echo 'export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"' > ~/.bashrc
    $ source ~/.bashrc
    ```
    Note: If you use another shell such as zsh, adjust acccordingly
// ANCHOR_END: steps
/* ANCHOR: all */
