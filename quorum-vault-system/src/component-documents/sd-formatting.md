# SD Formatting
// ANCHOR: steps
1. Insert a fresh SD card into the SD card slot or connect it via a USB card reader to your computer

    * microSD or standard SD card can be used

1. Launch a terminal 

1. {{ #include finding-device-name.md: content }}  

1. Before formatting, you need to unmount the SD card. Replace `/dev/sdX1` with the actual partition name you identified in the previous step:

    * `sudo umount /dev/sdX1`

1. Use the mkfs command to format the SD card. You can choose the file system type (e.g., vfat for FAT32, ext4, etc.). Replace /dev/sdX with the actual device name (without the partition number):

    * `sudo mkfs.vfat /dev/sdX`

1. You can verify that the SD card has been formatted by running lsblk again or by checking the file system type:

    * `lsblk -f`

1. Once formatting is complete, you can safely remove physically or eject the SD card:

    * `sudo eject /dev/sdX`
//ANCHOR_END:steps