/* ANCHOR: all */
# Git Repository Initialization

This document explains how a git repository should be set up in order to guarantee authenticity and non-repudiation of data.

Git is used because it permits cryptographic singing of commits using PGP, as well as historical changes to a set of data.

## Procedure: Setting up Repository
// ANCHOR: procedure
1. Create a git repository using a git system such as Forgejo, GitLab, GitHub etc.

1. Set appropriate permissions to limit who can write to the repository.

    * Require that all commits are signed using well known PGP keys

1. Optionally set up a cron job that periodically pulls the data from the repository as a backup.
// ANCHOR_END: procedure
/* ANCHOR_END: all */

