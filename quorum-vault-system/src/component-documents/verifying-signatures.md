# Verifying Signatures

When building and downloading software it is essential to verify signatures to ensure its integrity. It is also important to verify that the latest commit, and ideally that all commits that are being used to build from are verified to have signatures from trusted keys. This can be done using `git verify-commit HEAD` or similar. A script like below can be modified to check for trusted keys for all commits:

```bash
#!/bin/bash

mapfile -t trusted_keys < trusted_keys.txt

is_trusted_key() {
    local key="$1"
    for trusted_key in "${trusted_keys[@]}"; do
        if [[ "$key" == "$trusted_key" ]]; then
            return 0
        fi
    done
    return 1
}

git rev-list --all | while read commit; do
    if git verify-commit "$commit" > /dev/null 2>&1; then
        key_id=$(git show "$commit" | grep 'gpgsig' | awk '{print $NF}')
        
        if ! is_trusted_key "$key_id"; then
            echo "$commit: Signed but NOT by a trusted key ($key_id)"
        fi
    else
        echo "$commit: Not signed"
    fi
done
```

Verification of software depends on two primary aspects:

* Ensuring that the hash of a binary matches some point of reference, for example the same binary previously built by a trusted team member, or a hash hosted alongside the software in the download location.

* Ensuring that signatures alongside hashes are from trusted asymmetric keys (e.g PGP keys)

In order to achieve this, one must establish that specific keys are "well known" and can be trusted - that is, that they belong to a given individual. To achieve this, the best method is to exchange keys in person, but a combination of the following methods gives even higher confidence thresholds:

* Verifying the key in person

* Finding a reference to a public key on the individual's personal website

* Finding a reference to a public key on the individual's social media platforms

* Finding a keyoxide profile for a given public key

* Finding a reference to a public key on a company website

* Looking up popular key servers to see if a given individual is associated with it

Each point of reference allows us to build confidence that the key is indeed owned by an individual.

One other consideration is how the key is protected. If possible, find out how the individual manages their key. If the key is stored on a local machine, the trust level for that key should be low. If the individual always manages their keys in airgapped environments, and on HSMs, then a higher level of trust can be ascribed - although ultimately in most cases it's impossible to verify that the individual followed a given policy around key management.

One favorable method for ensuring that a key never got exposed is using built in cryptographic attestation that a key never left a TPM, such as the one offered by YubiKey. While this type of key setup has the downside of not being able to back it up, one could use a master key to sign such a key, authorizing it for use, while giving the flexibility to rotate if the hardware token is damaged or lost.