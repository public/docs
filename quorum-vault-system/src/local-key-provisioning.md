# Local Key Provisioning

This document contains instructions on how Operators collaborate to set up
QVS which requires an N-of-M quorum to be reconstituted. The encrypted shards
which result from this ceremony are stored in separate physical
[Locations](locations.md) which contain [Location Keys](glossary.md#location-key)
to which shards are encrypted, and whose passphrases are protected using
[Operator Keys](glossary#operator-key).


### Requirements

* [Smart Card](hardware.md#smart-cards): whatever number of smart
cards you would like to have seeded for each Operator, usually 2 per Operator is
recommended - one NitroKey 3 and 1 YubiKey Series 5.

* [Storage Devices](hardware.md#storage-device): as many storage
devices as you would like for backing up [Public Ceremony Artifacts](public-ceremony-artifact-storage.md)

* Storage Device loaded with
	* [airgap.iso](repeat-use-airgapos.md)
	* [airgap.iso.asc](repeat-use-airgapos.md)
	* [autorun.sh](autorun-sh-setup.md)

* All participants need Ceremony Notes which contain a record of which they
verified and wrote down themselves:
	* The SHA256 hash of airgap.iso
	* The SHA256 hash of autorun.sh

### Steps

1. Bring the Ceremony Machine and [Quorum Team](quorum-team.md) into the
established [Location](locations.md)

2. Ensure that no participants have brought digital devices other than ones
necessary for the ceremony. A faraday bag may be used to hold any such devices
for the duration of the ceremony.

3. Plug in a new Storage Device

4. Boot your Ceremony Machine using [Secure Boot Sequence](secure-boot-sequence.md)

5. As prompted plug in new Smart Cards

6. Once the ceremony is complete, make as many copies of the Storage Device
from Step 3 as desired.

7. Follow the [Physical Artifact Storage](physical-artifact-storage.md) guide
 for storage of the Operator Smart Cards and Location Smart Cards

8. Follow the [Public Ceremony Artifacts Storage](public-ceremony-artifact-storage.md)
guide for all public artifacts produced during the ceremony

