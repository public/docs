# Operator Key Provisioning

## Description
This guide can be used for provisioning Operator key pairs, and the output of
the ceremony is a set of the following for each Operator:
* Smart Card(s) seeded with PGP keys
* Storage Device with a backup of:
    * PGP key pair public key

## Requirements
* Smart Card(s): whatever number of smart cards you would like to have seeded
for each Operator, usually 2 per Operator is recommended - one NitroKey 3 and
1 YubiKey Series 5.

* [Storage Devices](hardware.md#storage-device): as many storage devices as you
would like for backing up [Public Ceremony Artifacts](public-ceremony-artifact-storage.md)

## Playbook

### Steps
This playbook allows the setup of any number of Operator Keys. For each Operator,
the steps that follow need to be repeated.

1. Bring the Ceremony Machine and [Quorum](selecting-quorum.md) team into the
established [Location](locations.md)

2. Boot your Ceremony Machine using [Secure Boot Sequence](secure-boot-sequence.md)

3. Plug in a new Storage Device

4. Run `keyfork wizard operator` TODO: this command is not part of `keyfork` yet

5. As prompted plug in new Smart Cards

6. Once the ceremony is complete, make as many copies of the Storage Device
from Step 3 as desired.

7. Follow the [Physical Artifact Storage](physical-artifact-storage.md) guide
 for storage of the Operator Smart Cards and Location Smart Cards

8. Follow the [Online Artifacts Storage](public-ceremony-artifact-storage.md)
guide for all public artifacts produced during the ceremony


