# Introduction

Quorum Vaulting System (QVS) is an open source system of playbooks and
tooling which facilitates the creation and maintenance of highly resilient
[quorum](glossary.md#quorum)-based key management systems based on a strict
[threat model](threat-model.md) which can be used for a variety of different
cryptographic algorithms. The system was designed and developed by
[Distrust](https://distrust.co), with the generous support of sponsors.

The basic premise of QVS is that primary cryptographic material akin to a root
certificate, called [Root Entropy (RE)](glossary.md#root-entropy-re), is generated
during a secure key derivation ceremony, and then used to derive chosen
cryptographic material via different algorithms such as PGP keys, digital asset
wallets, web certificates and more. 

Currently there is a set of an opinionated set of playbooks for working with OpenPGP and blockchains is in development, and will be extended to digital certificates, FIDO secrets and more in the future.

The RE is sharded using [Shamir's Secret Sharing (SSS)](glossary.md#shamirs-secret-sharing-sss)
to a [Quorum](glossary.md#quorum) in order to protect it from single points of
failure, requiring cooperation of multiple individuals - a quorum, and use of
cryptographic material stored in separate physical locations with significant
access controls in order to reconstruct the secret material, namely the RE.

## Use Cases

QVS can be used for a wide range of use-cases which span but are not limited
to:

* Deriving a PGP key pair whose public key can be used as a "one-way deposit
box" - for example a company can back up keys for their digital asset wallets by
encrypting them to the public key and storing the encrypted ciphertext blobs on
multiple cloud storage platforms, or on offline hard drives for redundancy.

* Deriving PGP keys for multiple individual users in a deterministic manner.

* Deriving wallets for digital assets using BIP-0032 style derivation as part of
a cold signing setup.

* Decrypting data in a secure, quorum protected, air-gapped environment.

* Generating digital certificates

## Playbooks

QVS can be set up by using a set of highly opinionated playbooks which outline
the process. The base documentation should be read in its entirety by all
participants of the ceremony in order to ensure that the system is well
understood by all to ensure that the integrity of the process is preserved and
enforced.

## Directives

The documentation uses directives in order to specify the importance of
adhering to parts of the specification according to [RFC2119](https://www.rfc-editor.org/rfc/rfc2119) and [RFC8174](https://www.rfc-editor.org/rfc/rfc8174).

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD",
"SHOULD NOT", "RECOMMENDED", "NOT RECOMMENDED", "MAY", and "OPTIONAL" in this
document are to be interpreted as described in BCP 14 [RFC2119] [RFC8174] when,
and only when, they appear in all capitals, as shown here.

## Method

The reader is encouraged to read through the entire body of documents which
should take approximately 30 minutes. If any parts are unclear, they may contact
Distrust for clarification, which is welcomed as it will aid in improving the
documentation.