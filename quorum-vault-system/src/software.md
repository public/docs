# Software
This page outlines the software used for setting up QVS.

## [[Stageˣ]](https://codeberg.org/stagex/stagex)

All software is built in a deterministic manner and reproduced by multiple
individuals on diverse hardware to minimize the risks associated with supply
chain attacks.

To achieve this, [Stageˣ] is used - a toolchain for building software using a
fully bootstrapped compiler, which itself is built deterministically, and
multi-reproduced.

[Stageˣ] was designed and developed by Distrust, with generous sponsorship from
Keyternal and Mysten Labs.

## [AirgapOS](https://git.distrust.co/public/airgap)

AirgapOS is an operating system built for those that want to be -really- sure
that sensitive cryptographic material is managed in a clean environment with an
"air gap" between the machine and the internet with high integrity on the supply
chain of the firmware and OS used. This OS is hardened and specifically designed
as an appliance for working with cryptographic material.

The software was developed by [Distrust](https://distrust.co) and has undergone
an [audit](https://git.distrust.co/public/airgap/src/branch/main/audits) by
Cure53 with no significant vulnerabilities found and has since then undergone
additional hardening.

The [AirgapOS Setup](repeat-use-airgapos.md) guides the user through verifying and
setting up AirgapOS on a bootable disk to use as part of the [Key Derivation
Ceremony](glossary.md#key-derivation-ceremony)

## [Keyfork](https://git.distrust.co/public/keyfork)

Keyfork is an opinionated and modular toolchain for generating and managing a
wide range of cryptographic keys offline and on Smart Cards from a shared
BIP-0039 mnemonic phrase. BIP-0039 phrases are used to calculate a BIP-0032
seed, which is used for hierarchical deterministic key derivation.

This software is the backbone for all cryptographic actions performed as part
of QVS. It was developed by [Distrust](https://distrust.co) and is included
with AirgapOS and has been audited by two firms, NCC and Cure53 with no
significant vulnerabilities found.


## [Icepick](https://git.distrust.co/public/icepick)

Icepick is a framework for rapidly developing applications to perform transfer and staking cryptocurrency operations. It works synergistically with `keyfork` which derives keys which are then used by `icepick`.