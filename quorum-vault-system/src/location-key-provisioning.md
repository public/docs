# Location Key Provisioning

## Description
This ceremony is for generating Location Keys. Location Keys are typically
stored in vaults as prescribed in the [Secure Storage Guidelines](secure-storage-guidelines.md).
Location Keys are keypairs to which the Root Entropy of a QVS is sharded. The
keypairs are stored exclusively on Smart Cards, and the PINs which protect the
Smart Cards are encrypted to Operator Keys.

## Requirements
* Smart Card(s): whatever number of smart cards you would like to have seeded
for each Operator, usually 2 per Operator is recommended - one NitroKey 3 and
1 YubiKey Series 5.

* [Storage Devices](equipment.md#storage-device): as many storage devices as you
would like for backing up [Public Ceremony Artifacts](public-ceremony-artifact-storage.md)

## Steps

1. Bring the Ceremony Machine and [Quorum Team](quorum-team.md) into the
established [Location](locations.md)

2. Boot your Ceremony Machine using [Secure Boot Sequence](secure-boot-sequence.md)
or the [One Time Use Airgap-OS](TODO)

3. Provision new key in the selected secure environment

    * Set system time to date greater than operator key creation but less than
    operator key expiry

        * `date -s "YYYY-MM-DD HH:MM:SS"`

    * Load your personal PGP certificate which will be used as the Operator
    Key in to the local keyring

        * `gpg --import /media/<operator_key_id>`

    * Generate the mnemonic:

        * `keyfork mnemonic generate --size 256 > mnemonic.txt`

    * Write the mnemonic on a small piece of paper as you will need to enter the
    words in the next step. After entering the words, set the piece of paper
    on fire (that's why it should be small enough - to make burning it easy)

    * In a new terminal window start `keyfork` daemon with the mnemonic:

        * `export KEYFORKD_SOCKET_PATH=/tmp/keyforkd.socket`

        * `keyfork recover mnemonic`

        * ctrl + z

        * `bg`

    * Derive PGP keypair:

        * `keyfork derive openpgp "Location Key: Distrust Disaster Recovery" > location.priv.asc`

    * Provision two YubiKey:

        * To get the `smart_card_id`: `oct list`

        * `oct admin --card <smart_card_id> import location.priv.asc`

        *  `keyfork mnemonic generate --size 256 | awk '{ print $1, $2, $3, $4, $5 }' > smart-card-pin.txt`

        * `oct pin --card <smart_card_id> set-user`

            * Enter the <smart_card_pin>

        * `oct pin --card <smart_card_id> set-admin`

            * Enter the <smart_card_pin>

    * Import PGP key into keyring

        * `gpg --import location.priv.asc`

    * Encrypt and back up the `mnemonic.txt`

        * `gpg -er <operator_key_id> mnemonic.txt`

        * `cp mnemonic.txt.gpg /media`

    * Encrypt and back up the `smart-card-pin`

        * `gpg -er <operator_key_id> smart-card-pin.txt`

        * `cp smart-card-pin.txt.gpg /media`

    * Export and back up `location.pub.asc`

        * `gpg --armor --export <location_key_id> > location.pub.asc`

        * `cp location.pub.asc /media`

    * Duplicate all backup files to a second SD card:

        * `mnemonic.txt.gpg`, `smart-card-pin.gpg`, `location.pub.asc`

    * Copy the `location.pub.asc` to the SD card that was used to bring over
    the operator key. This will be used to bring the public key to the Hybrid
    Ceremony Later on:

        * `cp location.pub.asc /media`

    * For posterity, delete all the generated assets before shutting down
    computer:

        * `rm -rf *`

4. Follow the [Physical Artifact Storage](physical-artifact-storage.md) guide
 for storage of the Operator Smart Cards and Location Smart Cards

5. Follow the [Public Ceremony Artifacts Storage](public-ceremony-artifact-storage.md)
guide for all public artifacts produced during the ceremony
