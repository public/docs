# Quorum Team

The Quorum Team is a team of individuals who are selected to perform different
roles related to a QVS. Some of the Quorum Team members have ongoing roles,
while others may participate in a partial manner.

Depending on the type of actions performed, some or all of the members of the
Quorum may be required to participate. For example, it is recommended that there
is at least 1 witness for all sensitive actions, but having 3 witnesses during
the primary Key Derivation Ceremony can give better assurances that the most
crucial part of the ceremony was properly conducted.

The roles which are required for the Quorum Team are the following:

## Operator
Operators are individuals who protect [Smart Cards](equipment.md) which
store a [GPG Key Certificate](glossary.md#gpg-certificate), which is used
for encrypting information, such as the [Location Key](glossary.md#location-key)
PINs as well as other [Ceremony Artifacts](reduntant-storage-of-ceremony-artifacts.md).
They are also individuals who participate in Ceremonies in order to Reconstitute
the key and perform actions with it.

## Controller
Controllers are an additional optional role which can be used to protect access
to the physical locations which hold the Location Keys. Some vaulting solutions
allow for requiring more than 1 individual in order to access a vault. Multiple
Controllers may be used to protect access to physical locations - according to
risk appetite.

## Witness
Witnesses are individuals who are familiar with the QVS specification, and can
ensure that the different aspects of the system are set up correctly, and
processes carried out as they should be. The main objective of the witnesses is
to monitor and attest that processes such as the ceremonies are done according
to the strictly prescribed rules.

## Courier
This role can be fulfilled by members of the Quorum Team and their main role is
getting materials to their designated locations - the main task being getting
the Location Keys securely to their physical location.