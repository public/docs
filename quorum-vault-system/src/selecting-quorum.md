# Selecting a Quorum

The backbone of QVS is a Quorum which is used to reconstitute or re-assemble
cryptographic material, and approve actions. Quorum is a general term referring
to a system which requires the collaboration of multiple individuals in order to
achieve something, and it is based on a Threshold which determines how many
Members of a Quorum there are in total, and the Quorum, which is how Members are
required to reach consensus.

The following is a simple Quorum example. Let's assume there are 3 trusted
individuals who will be part of Quorum and at least 2 of the Members of the
Quorum should be required to reach consensus. In that case the chosen Quorum
would be "2 of 3" - in other words, 2 of the total of 3 are required to achieve
consensus. These numbers may be adjusted in order to optimize risk tolerance
along two axis:

* Tolerating loss of X members aka "Bus Factor"

* Tolerating duress of Y members aka "Wrench Factor"

In a "2 of 3" Quorum, the **Bus Factor** is 1 as we can only afford to lose 1
member of the Quorum before the ability to reach consensus is lost permanently.
For that same Quorum, the **Wrench Factor** is 2, as an adversary has to use
their wrench on two different Quorum Members to force them to give them access
to their shards before the system is compromised (this is an over-simplification
as there are additional security controls in place such as the physical
locations which hold Location Keys, and as such the Wrench Factor is hardened).

It is recommended to use a "2 of 3" Threshold at a minimum, but many organizations
may choose to go with more resilient Threshold such as "3 of 5", "2 of 6", or
"5 of 7", depending on considerations pertaining to:

* Availability requirements

* Bus Factor requirements

* Wrench Factor requirements

