# Glossary

## Ciphertext

In cryptography, ciphertext is the result of encryption performed on plaintext
using an algorithm, called a cipher.

## Entropy

Entropy in cryptography refers to the measure of randomness or unpredictability
in data used for generating cryptographic keys and other security elements.

## Quorum Key Management (QVS)

A set of highly specified processes and tooling used for setting up a highly
resilient quorum-based key management system.

## Operator

An individual who manages an [Operator Key](#operator-key) which is used for
protecting the passphrase of a Location key and participates in different
aspects of the lifecycle management of the QVS system.

## Operator Key

An asymmetric key used for protecting the passphrase of a Location key

## Quorum

The chosen M of N threshold used to achieve "quorum", which is a type of
agreement or consensus between a group of individuals. In the context of
Shamir's Secret Sharing, it refers to the minimum number of shards required to
reassemble the [Root Entropy](#root-entripy-re).

### Wrench Factor

How many people are required to be put under duress (via smacking them with a
wrench repeatedly or otherwise to get access to their Shard) before the Disaster
Recover Key is compromised.

### Bus Factor

How many members of the Quorum can meet an untimely demise without irretrievably
locking access to the Distrust Quroum system.

## Root Entropy (RE)

The main [entropy](#entropy)/randomness which is used for hierarchical
deterministic key derivation of a variety of cryptographic algorithms.

## Shard

Cryptographic shard created using Shamir's Secret Sharing algorithm.

## Shamir's Secret Sharing (SSS)

An algorithm used to split cryptographic material into shards which can be
used to reassemble a secret. The shards can be combined according to a threshold
which is set at the time of initial sharding, expressed as M of N, or in other
words M shards of the total N shards in existence are required to reveal the
secret.

## Secure Compartmentalized Information Facility (SCIF)

## [RFC2119](https://www.rfc-editor.org/rfc/rfc2119) and [RFC8174](https://www.rfc-editor.org/rfc/rfc8174)

Specifications for keywords such as MUST, MUST NOT, SHOULD, SHOULD NOT, MAY etc.

## Workstation

Highly secure computer which is used for sensitive operations, typically in the
context of [Production Engineering](TODO).

#### Minimum

In order to set up a Workstation, as part of a [Production Engineering](TODO)
setup, a separate computer only used for carrying out sensitive operations
should be used.

#### Recommended

Setting up a computer with QubesOS is recommended as it makes it easy to create
purpose built environments with minimal surface area for attacks, on the OS, and
networking level.

## Air-Gapped

Hardware which has no access to radio frequency or cable based networking
capabilities in order to reduce area for surface attacks. Typically Air-Gapped
computers are laptops which are never connected to the internet or any other
networks, and have had their radio frequency hardware permanently disabled or
ideally removed.

## Key Derivation Ceremony

Highly specified process which is used to secure derivation of cryptographic
material, with a focus on ensuring no single party has access to the Disaster
Recovery Key. The ceremony includes witnesses who can help attest to the fact
that the ceremony was executed properly, as well as instructions on hardware,
software and location that should be used. Each step of the ceremony is
carefully planned, and executed accordingly.

## Location Key

Is a asymmetric key pair which is used for encrypting shards which are used to
re-assemble the Root Entropy. Location Keys are stored in [Locations](locations.md)
which adhere to a strict set of criteria to maximize their security. The location
smart card passphrase is encrypted to a Operator Key in order to secure access
to it.

## M of N

M is the minimum number of shards required to reassemble the secret, and N is
the total number of shards that exist. The minimum recommended threshold is
2-of-3.

## Organization

An organization which owns the QVS and is responsible for funding the setup and
maintenance. The organization is also responsible for ensuring that the
[Warehouse](#warehouse) is properly maintained in order to ensure that the
ciphertext blobs associated with the system are redundantly stored and
protected.

## Witness

An individual who attests the ceremony was performed according to specification
in order to have additional assurances the cryptographic material, most
importantly the Root Entropy was never exposed.

## Warehouse

* Online storage for encrypted data replicated across multiple providers
* All data in DR Warehouse can only be decrypted by the DR Key
* Tolerate loss of any single provider by duplicating data to all of them
* Storage backends can be any combination of the following:
    * S3 Compatible object stores:
        * AWS, Google Cloud, DigitalOcean, Azure, etc.
* [Version Control Systems](software.md#version-control-system-vcs):
* We tolerate a loss of all but one DR storage backend
* A minimum of three storage backends should be maintained

## MICE
A mnemonic device used in counterintelligence training to remind trainees of the four general motivations that could lead someone to commit treason, become an insider threat, or collaborate with a hostile agency or organization. It stands for Money, Ideology, Compromise, and Ego.