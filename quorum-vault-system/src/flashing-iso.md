# Flash ISO Image to a Storage Device

1. Select a new Storage Device which can be overwritten entirely

1. Find the name of the Storage Device using [this guide](storage-device-management.md#finding-a-storage-device-name)

1. Use the `dd` utility in the Terminal to flash AirgapOS to it. You will need
to replace `<your_storage_device>` with the name of your device.

```bash
sudo dd bs=4M if=~/airgap/dist/airgap.iso of=/dev/<your_thumb_drive> status=progress
```

In the example, the name of the device is `sda` so the complete command would look like this:

```bash
sudo dd bs=4M if=~/airgap/dist/airgap.iso of=/dev/sda status=progress
```

Once this step is complete, you have successfully set up a Storage Device
with AirgapOS.