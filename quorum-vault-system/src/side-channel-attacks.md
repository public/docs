# Side Channel Attacks

* Acoustic (DiskFiltration, Fansmitter, Acoustic Cryptanalysis, sonic cavity resonators, etc.)

* Optical (VisiSploit, xLED, eavesdropping via vibration analysis etc.)

* Thermal (BitWhisper)

* Electrical (Simple Power Analysis (SPA), Differential Power Analysis)

* Magnetic (ODINI)

* Electromagnetic (USBee)
    * Exfiltrating data via induction of power lines, water pipes, telephone lines, coaxial cable used for televisions and internet etc.

* Radio (NFCDrip, Vapor Trail, RFID, FM, Wifi, Bluetooth, GSM, SATA cable eavesdropping etc)

* Steganography
