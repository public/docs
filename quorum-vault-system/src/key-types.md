# PGP Key Types

## Personal PGP Keypair

Used for day to day operations such as signing keys being added to keychain, signing tamper evidence, signing transaction requests and approvals etc.

When bootstrapping a system, the initial PGP keys can be generated using [this guide](./generated-documents/all-levels/pgp-key-provisioning.md).

### Requirements

* MUST not be transferred

* MUST be generated offline

* MUST have the root key offline

* MUST have subkeys maintained on a smartcard

## Quorum PGP Keypair

Only used in ceremonies for decrypting shardfile material.

### Requirements

* MUST use smart-card within air-gapped ceremonies

* MUST not have PII attached to them

* MUST be generated in a witnessed ceremony

* MUST only be backed up to a quorum

* MUST not be transferred in level 4

* MAY be transferred in levels 1-3
