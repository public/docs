# Personal PGP Key Provisioning

## Requirements

* [AirgapOS SD card](../level-2/fixed-location/provisioner/provision-airgapos.md)
  * Provided by [Air-Gapped Bundle](../level-2/fixed-location/provisioner/air-gapped-bundle.md)
  * Alternative: Create your own from documentation in [AirgapOS Repository](https://git.distrust.co/public/airgap)

* AirgapOS Laptop
  * Provided by [Air-Gapped Bundle](../level-2/fixed-location/provisioner/air-gapped-bundle.md)
  * Alternative: Computer that can load AirgapOS ([compatibility reference](https://git.distrust.co/public/airgap#tested-models))

{{ #include ../../component-documents/linux-workstation.md:content }}

* 1+ Smart Card
  * At least 1 primary smart card
  * Any number of backup smart cards

* 1 Transfer SD card
  * Document will assume the card is labelled as "TRANSFER"

##  Process

1. If using pre-sealed Cold Bundle unseal as follows:

{{ #include ../../component-documents/tamper-evidence-methods.md:vsbwf-procedure-unsealing }}

1. Boot AirgapOS Laptop

{{ #include ../../component-documents/openpgp-setup.md:steps-keyfork}}

1. Power down AirgapOS Laptop

1. Switch to Linux Workstation

1. Attach SD card from AirgapOS machine

1. Attach smartcard provisioned with AirgapOS machine

1. Import newly generated public key into local keychain

    ```
    $ gpg --import /media/TRANSFER/*.pub.asc
    ```

{{ #include ../../component-documents/git-commit-signing.md:steps }}

1. Push new key material to Vaults repository

	a. Clone repository (if not done previously)
    ```
    $ git clone <vaults_repository_url> ~/vaults
    ```
	b. Checkout main branch
    ```
    $ cd ~/vaults
    $ git checkout main
    $ git pull origin main
    ```
	c. Commit and push modifications
    ```
    $ cp /media/TRANSFER/*.asc keys/all
    $ git add .
    $ git commit -S -m "add <name> pgp key"
    $ git push origin main
    ```

1. Communicate your new key fingerprint to all other participants:

    * Preferred: In person

    * Fallback: via two logically distinct online communications methods (e.g. encrypted chat, and video call)

1. Get confirmation they have imported your key to their keychains

    * e.g. `gpg --import <your_key_id>.asc`
    * Confirm this is done for keyrings on workstations used to interact with the Vaults repository
