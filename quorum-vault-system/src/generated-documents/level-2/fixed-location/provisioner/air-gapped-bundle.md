# Air-Gapped Bundle

## Requirements

{{ #include ../../basic-requirements.md:basic }}

* AirgapOS SD Card

* Air-gapped computer

## Procedure

{{ #include ../../../../component-documents/tamper-evidence-methods.md:vsbwf-procedure-sealing}}

1. Label the tamper proofed package as "Air-Gapped Bundle [num]", for example "Air-Gapped Bundle 2" if one already exists

1. Update inventory to indicate a new air-gapped bundle is available