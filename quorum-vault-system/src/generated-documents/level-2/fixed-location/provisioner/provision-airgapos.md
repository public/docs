# AirgapOS

## Requirements

{{ #include ../../basic-requirements.md:requirements }}

* Tamper proofing evidence (photographs)

* [SD Card Pack(s)](../procurer/procure-sd-card-pack.md) 

* High Visibility Storage

* 2 Computers

    * 1 computer should be able to boot AirgapOS ([compatibility reference](https://git.distrust.co/public/airgap#tested-models))

## Procedure

1. Turn on one of the computers - this one will be used for writing the SD cards

1. Build the software according to the [readme](https://git.distrust.co/public/airgap) in the repository. 

1. Use the `make reproduce` command

1. Unseal the SD Card Pack

{{ #include ../../../../component-documents/tamper-evidence-methods.md:vsbwf-procedure-unsealing }}

1. Label each SD card that will be used "AirgapOS [date]"

1. Place all the SD cards into High Visibility Storage

1. Retrieve a labelled SD card from High Visibility Storage, and plug it into the computer where AirgapOS will be built

1. {{ #include ../../../../component-documents/finding-device-name.md:content }}

1. Flash `airgap.iso` to an SD Card:

    * `dd if=out/airgap.iso of=/dev/<device_name> bs=4M status=progress conv=fsync`

1. Reset the computer, and boot the SD card 

1. Once booted, the card needs to be locked using `sdtool` which is available in `AirgapOS`:

    * Note: the device will not mount as a proper block device on QubesOS so a different OS has to be used where the device appears as /dev/mmcblk<num>

1. `./sdtool /dev/<device_name> permlock`

1. Once burned, unplug the SD card

1. Plug the SD card into a different computer from the one that was used to write the SD card

1. Boot the computer

1. Open a terminal

1. Verify the card can't be written to:

    * `echo "42" | dd of=/dev/<device_name>`        

{{ #include ../../../../component-documents/tamper-evidence-methods.md:vsbwf-procedure-sealing }}