# Provisioner

The provisioner is responsible for:

* Provisioning hardware

* Provisioning SD Cards (AirapOS, Ceremony etc.)

* Provisioning bundles (e.g Air-Gapped bundle)

## Procedures

* [Provision AirgapOS](./provision-airgapos.md)
* [Provision Computer](./procure-computer.md)
    * Requires tamper proofing equipment to be available
* [Provision Air Gapped Bundle](./provision-air-gapped-bundle.md)
    * Requires operators to have smart cards with PGP keys, tamper proofing equipment, AirgapOS SD card
