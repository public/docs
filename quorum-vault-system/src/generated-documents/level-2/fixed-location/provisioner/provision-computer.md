# Provision Computer

## Requirements

{{ #include ../../basic-requirements.md:requirements }}

* Tamper proofing evidence (photographs)

* Non-provisioned computer 

## Procedure

1. Unseal a tamper proofed laptop

{{ #include ../../../../component-documents/tamper-evidence-methods.md:vsbwf-procedure-unsealing }}

1. Remove all radio cards, storage drive, speakers, and microphone using standard industry laptop repair tactics

{{ #include ../../../../component-documents/tamper-evidence-methods.md:vsbwf-procedure-sealing }}

1. Apply a new label which indicates the laptop has been provisioned (include date, and any other desired metadata such as a unique ID (e.g Laptop #4))

1. Place the provisioned laptop in inventory

1. Update inventory to reflect that this hardware has been provisioned, and including the metadata in the `description.txt` for that item according to the [inventory repository structure](../procurer/create-inventory-repository.md)