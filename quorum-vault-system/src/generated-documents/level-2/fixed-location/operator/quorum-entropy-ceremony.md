# Quorum Entropy Ceremony

This is a ceremony for generating entropy which is used to derive Quorum PGP keys, load them into smart cards and shard entropy to them.

## Requirements

{{ #include ../../operator-requirements.md:requirements }}

* [Ceremony SD Card](./ceremony-sd-card-provisioning.md)

* [SD Card Pack](../procurer/procure-sd-card-pack.md)

* `N` Smart Cards in the chosen `M of N` quorum

* High Visibility Storage: plastic container or bag that's used to keep items while not in use in a visible location like the middle of a desk.

## Procedure

{{ #include template-ceremony-setup.md:content }}

1. Run the relevant keyfork operation to perform the ceremony:

    * Replace the following values: \<M>, \<N>, <number_of_smart_cards_per_operator>, <pgp_user_id> with appropriate values
	```
	$ keyfork mnemonic generate --shard-to-self shardfile.asc,threshold=<M>,max=<N>,cards_per_shard=<number_of_smartcards_per_operator>,cert_output=keyring.asc --derive='openpgp --public "Your Name <your@email.co>" --output certificate.asc'
	```

1. Unseal an SD card pack by following tamper proofing steps:

{{ #include ../../../../component-documents/tamper-evidence-methods.md:vsbwf-procedure-unsealing}}

1. Place all unsealed SD cards into High Visibility Storage

1. Plug in the Ceremony SD card

1. Back up the files
    ```
    $ cp shardfile.asc /media/vaults/<namespace>/
    $ cp keyring.asc /media/vaults/<namespace>/
    $ cp certificate.asc /media/vaults/<namespace>/
    $ cp -r /media/vaults /root/
    ```

1. To create additional backups of the updated `vaults` repository, plug in SD cards one at a time and use following steps to back up ceremony artifacts

    1. Plug in fresh SD card

    1. `cp -r /root/vaults /media/`

    1. Unplug the SD card

    1. Label the SD card "Ceremony [date]"

    1. Place the SD card in High Visibility Storage

1. Power down the air-gapped machine

1. Transfer the ceremony artifacts to online linux workstation using one of the SD cards and commit the changes made to the `vaults` repository that's on the Ceremony SD card

{{ #include ../../../../component-documents/git-basics.md:content }}

1. Gather all the original items that were in the air-gapped bundle:

    * Air-gapped computer

    * AirgapOS SD card

{{ #include ../../../../component-documents/tamper-evidence-methods.md:vsbwf-procedure-sealing}}
