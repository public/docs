# Export Namespace Mnemonic

## Requirements

{{ #include ../../operator-requirements.md:requirements }}

* [SD Card Pack](../procurer/procure-sd-card-pack.md)

* [Ceremony SD Card](../operator/ceremony-sd-card-provisioning.md) 

* [High Visibility Storage](TODO): plastic container or bag that's used to keep items while not in use in a visible location like the middle of a desk.

## Procedure

1. Enter the designated location with the quorum of operators and all required equipment

1. Lock access to the location - there should be no inflow or outflow of people during the ceremony

1. Place Ceremony SD card in High Visibility Storage

1. Retrieve sealed Air-Gapped bundle, polaroid of tamper evidence, and online laptop from locked storage

{{ #include ../../../../component-documents/tamper-evidence-methods.md:vsbwf-procedure-unsealing}}

1. Place all contents except for the laptop into High Visibility Storage

1. Retrieve AirgapOS SD card and plug it into the air-gapped machine

1. Boot the computer  

1. Unplug the AirgapOS SD card and place it in High Visibility Storage

1. Retrieve Ceremony SD card from High Visibility Storage and plug it into the air-gapped machine 

1. Recover the mnemonic from an existing shardfile

    * `keyfork shard combine /media/vaults/<namespace>/shardfile.asc | keyfork-mnemonic-from-seed > mnemonic.txt`

1. Follow on screen prompts

1. Unplug the Ceremony SD card and place it in High Visibility Storage

1. Unseal the SD Card Pack 

{{ #include ../../../../component-documents/tamper-evidence-methods.md:vsbwf-procedure-unsealing}}

1. Put the mnemonic on an SD card for transport or use `cat` command to output it in the terminal for entry into a hardware wallet or otherwise

    * WARNING: if displaying on screen, ensure nothing else can see the mnemonic. It is recommended to cover the operator and the machine with a blanket to obstruct the view of the screen.

1. Shut down the air gapped machine

1. Gather all the original items that were in the air-gapped bundle:

    * Air-gapped computer

    * AirgapOS SD card

{{ #include ../../../../component-documents/tamper-evidence-methods.md:vsbwf-procedure-sealing}}