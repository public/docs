# Decrypt Namespace Secret

## Requirements

{{ #include ../../operator-requirements.md:requirements }}

* [Ceremony SD Card](../operator/ceremony-sd-card-provisioning.md)

* [High Visibility Storage](TODO): plastic container or bag that's used to keep items while not in use in a visible location like the middle of a desk.

## Procedure

{{ #include template-ceremony-setup.md:content }}

1. Retrieve Ceremony SD Card from High Visibility Storage and plug it into the machine

1. Copy the Ceremony SD Card contents to machine

    * `cp -r /media/vaults /root/`

1. Start `keyfork` using the relevant Shardfile:
	```
	$ keyfork recover shard --daemon /root/vaults/<namespace>/shardfile.asc
	```
    * Follow on screen prompts

1. Derive the OpenPGP root certificate:
	```
    $ keyfork derive openpgp > secret_key.asc
	```
1. Decrypt the secret material:

    * `sq decrypt --recipient-file secret_key.asc < encrypted.asc --output decrypted`

1. Proceed to transfer the secret (`decrypted`) to desired location such as hardware wallet, power washed chromebook (via SD card) etc.

1. Shut down the air gapped machine

1. Gather all the original items that were in the air-gapped bundle:

    * Air-gapped computer

    * AirgapOS SD card

{{ #include ../../../../component-documents/tamper-evidence-methods.md:vsbwf-procedure-sealing}}
