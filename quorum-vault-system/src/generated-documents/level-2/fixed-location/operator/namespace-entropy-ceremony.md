# Namespace Entropy Ceremony

This is a ceremony for generating and sharding entropy to a set of existing Quorum Keys.

## Requirements

{{ #include ../../operator-requirements.md:requirements }}

* [SD Card Pack](../procurer/procure-sd-card-pack.md)

* [Ceremony SD Card](../operator/ceremony-sd-card-provisioning.md)

* [High Visibility Storage](TODO): plastic container or bag that's used to keep items while not in use in a visible location like the middle of a desk.

## Procedure

{{ #include template-ceremony-setup.md:content }}

1. Plug the Ceremony SD card into the machine

1. Run the command to generate new entropy and shard it to quorum of public certificates of the input shardfile:

    * Replace the values: <path_to_input_shard>, <pgp_user_id>
	```
    $ keyfork mnemonic generate --shard-to <path_to_input_shard>,output=shardfile.asc --derive='openpgp --public "Your Name <your@email.co>" --output certificate.asc'
	```

1. Unseal an SD card pack

{{ #include ../../../../component-documents/tamper-evidence-methods.md:vsbwf-procedure-unsealing}}

1. Place all unsealed SD cards into High Visibility Storage

1. Plug in the Ceremony SD card

1. Back up the files
    ```
    $ cp shardfile.asc /media/vaults/<namespace>/
    $ cp certificate.asc /media/vaults/<namespace>/
    $ cp -r /media/vaults /root/
    ```

1. To create additional backups of the updated `vaults` repository, plug in SD cards one at a time and use following steps to back up ceremony artifacts

    1. Plug in fresh SD card

    1. `cp -r /root/vaults /media/`

    1. Unplug the SD card

    1. Label the SD card "Ceremony [date]"

    1. Place the SD caard in High Visibility Storage

1. Power down the air-gapped machine

1. Transfer the ceremony artifacts to an online machine using one of the SD cards and commit the changes made to the `vaults` repository that's on the Ceremony SD card

{{ #include ../../../../component-documents/git-basics.md:content }}

1. Gather all the original items that were in the air-gapped bundle:

    * Air-gapped computer

    * AirgapOS SD card

{{ #include ../../../../component-documents/tamper-evidence-methods.md:vsbwf-procedure-sealing}}

