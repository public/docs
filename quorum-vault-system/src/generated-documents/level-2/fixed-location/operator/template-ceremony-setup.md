/* ANCHOR: all */
// ANCHOR: content
1. Enter the designated location with required personnel and equipment

1. Lock access to the location - there should be no inflow or outflow of people during the ceremony

1. Retrieve Air-Gapped Bundle and polaroid tamper evidence from locked storage

{{ #include ../../../../component-documents/tamper-evidence-methods.md:vsbwf-procedure-unsealing}}

1. Place all materials except for the laptop into High Visibility Storage

1. Retrieve AirgapOS SD card from High Visibility Storage and plug it into air-gapped laptop 

1. Turn on the machine

1. Once booted, remove the AirgapOS SD card and place it into High Visibility Storage
// ANCHOR_END: content
/* ANCHOR_END: all */