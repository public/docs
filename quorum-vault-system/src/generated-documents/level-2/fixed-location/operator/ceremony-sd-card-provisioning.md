# Ceremony SD Card Provisioning

## Requirements

* [SD Card Pack](../procurer/procure-sd-card-pack.md)

* [Personal PGP Keys](/key-types.html#personal-pgp-keypair)

{{ #include ../../../../component-documents/linux-workstation.md:content }}

## Procedure

1. Turn on the computer

1. Open the SD Card Pack 

{{ #include ../../../../component-documents/tamper-evidence-methods.md:vsbwf-procedure-unsealing }}

1. Plug in a fresh SD card into computer

1. Navigate to the ceremony repository for the ceremony being executed

* {{ #include ../../../../component-documents/finding-device-name.md:content }}

1. Write the ceremony repo data to the SD card:

    `sudo cp -r vaults/ /media`

1. Unplug the SD card 

1. Turn off the computer