# Encrypt Wallet to Namespace Key

Procedure for importing an arbitrary secret (raw key, mnemonic, state secrets) into a Namespace.

## Requirements

* [Namespace OpenPGP Certificate]() 

    * It can be on an SD card or accessed online

## Procedure

1. Access machine which has the secret that should be encrypted available

    * If not on a computer, but a hardware wallet or otherwise, perform the steps on a air-gapped machine

1. Encrypt the secret to certificate:

    * `sq encrypt --for-file <certificate> <file_to_encrypt> --output encrypted.asc` 

1. Once encrypted, name the file appropriately and add it to an `artifacts/` directory in the appropriate namespace subdirectory in the `vaults` repository

{{ #include ../../../../component-documents/git-basics.md:content }}

