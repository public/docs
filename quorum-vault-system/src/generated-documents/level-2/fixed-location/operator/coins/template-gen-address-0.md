/* ANCHOR: all */
// ANCHOR: content
1. Retrieve AirgapOS SD card and plug it into the air-gapped machine

1. Turn on air-gapped machine

1. Unplug the AirgapOS SD card and place it in High Visibility Storage

1. Retrieve Ceremony SD card from High Visibility Storage and plug it into the air-gapped machine

1. Copy the `vaults` repository to the machine and switch to it
    ```
	$ cp -r /media/vaults /root/
	$ cd /root/vaults
    ```
1. Start Keyfork using the relevant Shardfile:
	```
	$ keyfork recover shard --daemon <namespace>/shardfile.asc
	```
    * Follow on screen prompts
1. If the desired `<coin>` directory doesn't exist for the namespace, create it:
	```
    $ mkdir -p <namespace>/<coin>
	```
1. Connect to the appropriate coin directory:
	```
    $ cd <namespace>/<coin>/
	```
1. Check what the latest address account is:
	```
	$ ls -la .
	```
1. Find what the latest number for the address is, and add 1 to it. This will be the new address account.

	* For example if the latest address file is 42, the new account_id would be 43. The addresses should start at `0`

	* Set an environment variable with the new account_id:
		```
		$ account_id=<num>
		```
// ANCHOR_END: content
/* ANCHOR_END: all */
