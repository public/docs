# Solana: Generate Address

## Requirements

{{ #include ../../../../operator-requirements.md:requirements }}

{{ #include ../../../../../../component-documents/linux-workstation.md:content }}

* [High Visibility Storage](TODO): plastic container or bag that's used to keep items while not in use in a visible location like the middle of a desk.

* [Quorum PGP key pairs](../../../key-types.md#quorum-pgp-keypair)

* [Ceremony SD card](../../../ceremony-sd-card-provisioning.md)

## Procedure

1. Enter the designated location with the quorum of operators and all required equipment

1. Lock access to the location - there should be no inflow or outflow of people during the ceremony

1. Place Ceremony SD card in High Visibility Storage

1. Retrieve sealed Air-Gapped bundle, polaroid of tamper evidence, and online laptop from locked storage

{{ #include ../../../../../../component-documents/tamper-evidence-methods.md:vsbwf-procedure-unsealing}}

1. Place all contents except for the laptop into High Visibility Storage

### Offline Machine: Generate Address

{{ #include ../template-gen-address-0.md:content }}

1. Generate a new address:
	```
    $ icepick workflow sol generate-address --account $account_id > $account_id.json
	```

{{ #include ../template-gen-address-1.md:content }}

### Online Machine: Generate Nonce Account

1. Turn on online machine

1. Retrieve the Ceremony SD card from High Visibility Storage and plug it into the computer

1. {{ #include ../../../../../../component-documents/finding-device-name.md:content }}

1. Copy the `vaults` repository from the Ceremony SD card:
	```
    $ cp -r /media/vaults ~/
	```
    * If the `~/vaults/` repository already exists, ensure it doesn't have any changes that haven't been committed, then remove it using `sudo rm -rf ~/vaults` before re-running the previous step

1. Ensure `icepick` is available on system

    * Follow steps from [installation guide](TODO)

1. Change directory into the desired \<namespace>/\<coin> directory:
	```
    $ cd ~/vaults/<namespace>/<coin>
	```
1. Select which account you are creating the delegate address by viewing the appropriate \<namespace>/\<coin>/ directory:
	```
    $ ls -la .
	```
1. Once you have selected the appropriate account, set the account_id variable:
	```
    $ account_id=<num>
	```
1. Use `icepick` to generate nonce account:

	* If using a non-`mainnet-beta` cluster, be sure to provide the `--cluster` argument

    * Set `icepick` config file:
	```
    $ export ICEPICK_CONFIG_FILE=<path_to_icepick_repositry>/icepick.toml`
	```
	```
    $ icepick workflow sol generate-nonce-account --input-file $account_id.json > $account_id-na.json
	```
    * Repeat command if returned message is "The transaction was possibly not received by the cluster."

1. Fund the wallet displayed on-screen with 0.01 SOL

    * Once the funding is done, the nonce account will be created

1. Stage, commit, sign and push the changes:
    ```
    $ git add .
    $ git commit -m -S "<message>"
    $ git push origin HEAD
    ```
### Sealing

1. Gather all the original items that were in the air-gapped bundle:

    * Air-gapped computer

    * AirgapOS SD card

{{ #include ../../../../../../component-documents/tamper-evidence-methods.md:vsbwf-procedure-sealing}}

