# Kyve: Generate Address

## Requirements

{{ #include ../../../../operator-requirements.md:requirements }}

{{ #include ../../../../../../component-documents/linux-workstation.md:content }}

* [High Visibility Storage](TODO): plastic container or bag that's used to keep items while not in use in a visible location like the middle of a desk.

* [Quorum PGP key pairs](../../../key-types.md#quorum-pgp-keypair)

* [Ceremony SD card](../../../ceremony-sd-card-provisioning.md)

## Procedure

1. Enter the designated location with the quorum of operators and all required equipment

1. Lock access to the location - there should be no inflow or outflow of people during the ceremony

1. Place Ceremony SD card in High Visibility Storage

1. Retrieve sealed Air-Gapped bundle, polaroid of tamper evidence, and online laptop from locked storage

{{ #include ../../../../../../component-documents/tamper-evidence-methods.md:vsbwf-procedure-unsealing}}

1. Place all contents except for the laptop into High Visibility Storage

### Offline Machine: Generate Address

{{ #include ../template-gen-address-0.md:content }}

1. Generate a new address:

    * `icepick workflow cosmos generate-address --chain-name kyve --account $account_id > $account_id.json`

	The option `--chain-name` can use `kyve`, `kaon` (testnet), and `korellia` (devnet)

{{ #include ../template-gen-address-1.md:content }}

### Online Machine: Updating Vaults Repository

1. Turn on online linux workstation

{{ #include ../../../../../../component-documents/git-basics.md:content }}
