
/* ANCHOR: all */
// ANCHOR: content
1. Sign the file using:

    * Import OpenPGP keys:

        * `gpg --import /media/<device_name>/vaults/keys/all/*.asc`

    * `gpg --detach-sign $account_id.txt`

1. You may repeat the previous steps, starting at the step where the `account_id` is set.

1. Once finished, copy the updated repository back to the Ceremony SD card:

    * `cp -rf /root/vaults /media/`

1. Shut down the air gapped machine

1. Unplug the Ceremony SD card and place it into High Visibility Storage
// ANCHOR_END: content
/* ANCHOR_END: all */