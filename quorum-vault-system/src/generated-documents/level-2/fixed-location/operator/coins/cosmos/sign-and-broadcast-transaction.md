# Solana: Sign and Broadcast Transaction

## Requirements

{{ #include ../../../../operator-requirements.md:requirements }}

{{ #include ../../../../../../component-documents/linux-workstation.md:content }}

* [High Visibility Storage](TODO): plastic container or bag that's used to keep items while not in use in a visible location like the middle of a desk.

* [Quorum PGP key pairs](../../../key-types.md#quorum-pgp-keypair)

* [Ceremony SD card](../../../ceremony-sd-card-provisioning.md)

## Procedure

1. Enter the designated location with the quorum of operators and all required equipment

1. Lock access to the location - there should be no inflow or outflow of people during the ceremony

1. Place Ceremony SD card in High Visibility Storage

1. Retrieve sealed Air-Gapped bundle, polaroid of tamper evidence, and online laptop from locked storage

{{ #include ../../../../../../component-documents/tamper-evidence-methods.md:vsbwf-procedure-unsealing}}

1. Place all contents except for the laptop into High Visibility Storage

### Online Machine: Acquire Nonce

1. Turn on online linux workstation

1. Retrieve the Ceremony SD card from High Visibility Storage and plug it into the computer

1. Get the nonce address for the address you are sending from by checking the appropriate \<namespace>/\<coin>/ directory.

	* e.g `vaults/<namespace>/<coin>/0-na.txt`

	* Set the nonce address variable:
	```
	$ nonce_address="$(cat vaults/<namespace>/<coin>/<account_id>-na.txt)"
	```
1. Set `ICEPICK_DATA_DIRECTORY`:

	{{ #include ../../../../../../component-documents/finding-device-name.md:content }}
	```
	$ export ICEPICK_DATA_DIRECTORY=/media/external/
	```

1. set `ICEPICK_CONFIG_FILE`
	```
	$ export ICEPICK_CONFIG_FILE=<path_to_icepick_repo>/icepick.toml`
	```
1. Run the command:
	```
	$ icepick workflow cosmos broadcast --chain-name <chain-name> --nonce-address=$nonce_address
	```
	* Await completion message before removing Ceremony SD card

	* This command will set the computer into "awaiting mode", which will broadcast the signed transaction from the SD card once it's plugged back in after the workflow payloads are signed on the offline machine

### Offline Machine: Create and Sign Transaction

1. Retrieve AirgapOS SD card and plug it into the air-gapped machine

1. Boot the computer

1. Unplug the AirgapOS SD card and place it in High Visibility Storage

1. Retrieve Ceremony SD card from High Visibility Storage and plug it into the air-gapped machine

1. {{ #include ../../../../../../component-documents/finding-device-name.md:content }}

1. Start Keyfork using the relevant Shardfile:
	```
	$ keyfork recover shard --daemon /media/<device_name>/vaults/<namespace>/shardfile.asc
	```
	* The Shardfile may be named something else. Use `find /media/<device_name>/vaults -type f -name '*shardfile*.asc'` to list all files.

1. Follow on screen prompts

1. Set `ICEPICK_DATA_DIRECTORY`:
	```
	$ export ICEPICK_DATA_DIRECTORY=/media/<device_name>
	```
1. Run the `icepick` command with the transaction payload

	* The payload is located in the appropriate vault location (e.g /media/<device_name>/vaults/<namespace>/ceremonies/<date>...)
	```
	$ icepick workflow --run-quorum <payload>.json --shardfile /media/<device_name>/vaults/<namespace>/shardfile.asc
	```
	* Follow on screen prompts

1. Unplug the Ceremony SD card and place it in High Visibility Storage

### Broadcast Transaction: Online Machine

1. Retrieve Ceremony SD from High Visibility Storage and plug it into online machine

1. The still running broadcast command on the online machine will broadcast the transaction automatically

1. The url that's found in the response after a successful broadcast should be reviewed and committed to the ceremony repository

1. Remove the transaction files in `ICEPICK_DATA_DIRECTORY`
	```
	$ rm $ICEPICK_DATA_DIRECTORY/transaction.json
	$ rm $ICEPICK_DATA_DIRECTORY/account_info.json
	```

1. Unplug the Ceremony SD card and place it in High Visibility Storage

### Repeat

1. You may repeat previous steps as many times as necessary to process all workflow payloads

## Finalization

1. Shut down online linux workstation

1. Shut down the air gapped machine

### Sealing

1. Gather all the original items that were in the air-gapped bundle:

    * Air-gapped computer

    * AirgapOS SD card

{{ #include ../../../../../../component-documents/tamper-evidence-methods.md:vsbwf-procedure-sealing}}
