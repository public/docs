# Procure Facility

1. Identify a location which is suitable for Level 2 ceremonies:

    * SHOULD be lockable to prevent inflow and outflow of persons during ceremonies

1. Procure an enclosure for locking equipment. A simple lockbox or a safe can be used. It should be at least large enough to fit several laptops, with some extra room.

1. Designate the location as the facility for conducting ceremonies and update documentation and policies to reflect this
