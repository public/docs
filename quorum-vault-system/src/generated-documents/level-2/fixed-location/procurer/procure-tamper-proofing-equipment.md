# Procure Tamper Proofing Equipment

The facility will require tamper proofing equipment which will be used to tamper proof items before they are stored in inventory.

These items don't require dual custody and can be purchased at any location.

### Vacuum Sealer, plastic roll, filler

{{ #include ../../../../component-documents/tamper-evidence-methods.md:vsbwf-equipment}}

### Digital camera

{{ #include ../../hardware.md:camera-models}}

### Polaroid camera

{{ #include ../../../../component-documents/tamper-evidence-methods.md:polaroid-cameras}}

### Label Printer

There are two options:

* Hand-held label printer with a built in keyboard

* Non-standalone label printer that needs a computer to send it the file to print

#### Examples

* [Brother P-Touch PT- D610BT Business Professional Connected Label Maker ](https://www.amazon.com/Brother-Business-Professional-Connected-Bluetooth%C2%AE/dp/B0B1KZJXPG/ref=sr_1_4)

* [Bluetooth Thermal Label Printer](https://www.amazon.com/LabelRange-Bluetooth-Wireless-Shipping-Packages/dp/B0DFC9GB5D/ref=sr_1_1_sspa)