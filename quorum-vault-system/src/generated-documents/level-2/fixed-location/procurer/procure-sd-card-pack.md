# Procure SD Card Pack

## Requirements

{{ #include ../../basic-requirements.md:requirements }}

* 5 Fresh SD card(s) per booster pack 

* High Visibility Storage

* Sealable plastic bag is required for this procedure:

    * {{ #include ../../../../component-documents/hardware-models.md:sealable-plastic-bags }}


## Procedure

{{ #include ../../../../component-documents/hardware-procurement-and-chain-of-custody.md:steps}}

1. Remove packaging from each SD card, and place them into High Visibility Storage

1. Select 5 SD cards to be tamper proofed from High Visibility Storage

{{ #include ../../../../component-documents/tamper-evidence-methods.md:vsbwf-procedure-sealing }}

1. Label the tamper proofed package "SD Card Pack [date]"
