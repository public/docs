# Procurer

The procurer is responsible for:

* Procuring equipment

    * [Tamper proofing equipment](procure-tamper-proofing-equipment.md)

    * [Hardware](procure-hardware.md) (computers, sd cards, sd card adapters, smart cards, cameras etc.)

* Creating and maintaining the [Inventory](create-inventory-repository.md)

* Ensuring equipment is properly tamper proofed

* Minimizing hardware supply chain security risks

## Order of Operations 

1. Provisioning [Personal PGP Keys](../../../all-levels/pgp-key-provisioning.md)

1. Procuring a [facility](./procure-facility.md)

1. Creating a [Inventory repository](create-inventory-repository.md)

1. Procuring [tamper proofing equipment](./procure-tamper-proofing-equipment.md)

1. Procuring [hardware](./procure-hardware.md)

    * Laptops

    * SD cards

    * SD card USB adapters

    * Smart cards
