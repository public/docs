# Hardware Procurement

## Requirements 

{{ #include ../../basic-requirements.md:requirements }}

* Sealable plastic bag is required for this procedure:

    * {{ #include ../../../../component-documents/hardware-models.md:sealable-plastic-bags }}

## Procedure: Local Procurement

{{ #include ../../../../component-documents/hardware-procurement-and-chain-of-custody.md:steps}}

## Procedure: Online Procurement

1. Select a well known and reputable supplier. Establishing a relationship with a hardware supplier that has a reputation for privacy, supply chain security is preferred.

2. Order the supplies to a registered mailbox, to prevent exposing your organization's location

## Tamper Proofing

All hardware: 

* MUST be procured using dual custody methods

* MUST be tamper proofed using vacuum sealing / stored in tamper evident vault

* MUST be properly labelled

* MUST be added to cryptographically signed inventory

### Procedure

{{ #include ../../../../component-documents/tamper-evidence-methods.md:vsbwf-procedure-sealing }}

## Equipment Models

### Computers Models

For [Level 2](../../../../threat-model.md#level-2) security, air-gapped computers which are used for cryptographic material management and operations are required.

{{ #include ../../hardware.md:computer-models }}

### SD Cards & Adapters

SD cards can be tamper proofed in packs of 4 to reduce the amount of tamper proofing that needs to be done.

Any high quality SD equipment can be used but below are some recommended products:

{{ #include ../../../../component-documents/hardware-models.md:sd-models }}

### Smart Cards

{{ #include ../../../../component-documents/hardware-models.md:smart-cards }}