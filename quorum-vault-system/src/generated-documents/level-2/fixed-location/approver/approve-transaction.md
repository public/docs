# Approver - Approve Transaction

The approver is responsible for verifying a transaction proposed by a [proposer](../../../../system-roles.md).

## Requirements

* [Quorum PGP Key](../operator/quorum-entropy-ceremony.md)

{{ #include ../../../../component-documents/linux-workstation.md:content }}

* [SD Card Pack](../provisioner/provision-sd-card.md)

* [Air-Gapped Bundle](../provisioner/air-gapped-bundle.md)

	* The approver should print photographic evidence from digital cameras which is stored in a PGP signed repository. The photographs should be of the top and underside of the vacuum sealed object.

	* The approver should verify the commit signatures of the photographs they are printing against a list of permitted PGP keys found in the `vaults` repo

* Clone the [Vaults Repository](../../../all-levels/create-vaults-repository.md) for your organization to the machine

## Procedure

1. Turn on online linux workstation

1. Pull the latest changes from the `vaults` repository

1. Unseal the SD Card Pack

{{ #include ../../../../component-documents/tamper-evidence-methods.md:vsbwf-procedure-unsealing}}

1. Plug a fresh SD card into the online linux workstation

1. Save the `vaults` repository to the SD card, referred to as the Ceremony SD card

1. Unplug the Ceremony SD card

1. Unseal the tamper proofed bundle

{{ #include ../../../../component-documents/tamper-evidence-methods.md:vsbwf-procedure-unsealing}}

1. Insert the AirgapOS SD card into the airgapped machine and turn it on

1. Once booted, unplug the AirgapOS SD card

1. Plug in the Ceremony SD card

1. {{ #include ../../../../component-documents/finding-device-name.md:content }}

1. Copy the git repo locally from the Ceremony SD card and change into it
	```
	$ cp -r /media/vaults /root/vaults
	$ cd /root/vaults
	```
1. Plug in the Operator smart card

1. Verify the existing signatures and add your own signature:

	* `icepick workflow --add-signature-to-quorum <namespace>/ceremonies/<date>/payload_<num>.json --shardfile <shardfile>.asc`

1. {{ #include ../../../../component-documents/finding-device-name.md:content }}

1. Copy the updated vaults repo to the SD card

	* `cp -r /root/vaults /media`

1. Unplug the SD card from the air-gapped machine

1. Plug in the SD card into the online linux workstation

1. {{ #include ../../../../component-documents/finding-device-name.md:content }}

1. Copy the updated repository locally and change into it:
	```
	$ cp -r /media/vaults ~/
	$ cd ~/vaults
	```
1. Stage, sign, commit and push changes to the ceremonies repository:
	```
    $ git add <namespace>/ceremonies/<date>/payloads/*
	$ git commit -S -m "add payload signature for payload_<num>.json"
	$ git push origin main
	```
1. Tamper proof the AirgapOS and Air-gapped laptop

{{ #include ../../../../component-documents/tamper-evidence-methods.md:vsbwf-procedure-sealing}}
