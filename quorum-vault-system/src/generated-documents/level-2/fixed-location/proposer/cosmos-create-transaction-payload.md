# Cosmos: Create Transaction Payload

{{ #include template-create-tx-0.md:content }}

1. Use icepick to generate and sign the payload by running one of the following available workflows:

	All commands in the following sections take `--chain-name` and (with the
	exception of `withdraw-rewards`) `--asset-name`. These are the "Chain" and
	"Asset" fields from the table below. For example, Kyve Mainnet tokens would
	be `--chain-name kyve --asset-name KYVE`. Seda Mainnet tokens would be
	`--chain-name seda --asset-name SEDA`. The table below documents all
	currently-supported combinations of chain and primary asset.

	| Chain Name | Chain | Asset |
	| ---------- | ----- | ----- |
	| Seda | seda | SEDA |
	| Seda Devnet | seda-devnet | SEDA |
	| Kyve | kyve | KYVE |
	| Kyve Testnet | kaon | KYVE |
	| Kyve Devnet | korellia | KYVE |

	Additionally, there may be some difficulty broadcasting a transaction due
	to the amount of gas consumed, as each Cosmos chain may have different
	computation power available. The option `--gas-factor` may be set to a
	number to multiply the gas by, such as `1.2`, to increase the amount of gas
	for a transaction. The default value is `1`, and may be omitted if desired.
	A value lower than 1 is not recommended.

	#### Stake
	Stake coins on the provided chain towards a validator operator's address.

	`$ icepick workflow cosmos stake --delegate-address <delegate-address> --validator-address <validator-address> --chain-name <chain-name> --asset-name <asset-name> --asset-amount <asset-amount> --gas-factor <gas-factor> --export-for-quorum --sign`

	#### Transfer
	Transfer coins on the cosmos blockchain.

	`$ icepick workflow cosmos transfer --from-address <from-address> --to-address <to-address> --chain-name <chain-name> --asset-name <asset-name> --asset-amount <asset-amount> --export-for-quorum --sign`

	#### Withdraw
	Withdraw staked coins from a validator. Staked coins may be held for an unbonding period, depending on the chain upon which they are staked.

	`$ icepick workflow cosmos withdraw --delegate-address <delegate-address> --validator-address <validator-address> --chain-name <chain-name> --asset-name <asset-name> --gas-factor <gas-factor> --export-for-quorum-sign`


	#### Withdraw Rewards
	Withdraw rewards gained from staking to a validator.

	`$ icepick workflow cosmos withdraw-rewards --delegate-address <delegate-address> --validator-address <validator-address> --chain-name <chain-name> --gas-factor <gas-factor> --export-for-quorum-sign`

{{ #include template-create-tx-1.md:content }}
