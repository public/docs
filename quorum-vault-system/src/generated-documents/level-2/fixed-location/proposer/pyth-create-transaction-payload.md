# Solana: Create Transaction Payload

{{ #include template-create-tx-0.md:content }}

1. Use icepick to generate and sign the payload by running one of the following available workflows:

	#### Transfer Token
	Transfer Pyth on Solana blockchain.

	`$ icepick workflow sol transfer-token --from-address <from-address> --to-address <to-address> --token-name PYTH --token-amount <token-amount> --export-for-quorum --sign`

{{ #include template-create-tx-1.md:content }}
