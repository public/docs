/* ANCHOR: all */
// ANCHOR: content
1. Copy the updated ceremonies repo to the SD card
	```
	$ cp -r /root/vaults /media
	```
1. Transfer the SD card from the air-gapped machine to the online machine

1. {{ #include ../../../../component-documents/finding-device-name.md:content }}

1. Copy the updated repository locally and switch to it:
	```
	$ cp -r /media/vaults ~/
	$ cd ~/vaults
	```
1. Stage, sign, commit and push the changes to the ceremonies repository:
	```
    $ git add <namespace>/ceremonies/<date>/payloads/*
	$ git commit -S -m "add payload signature for payload_<num>.json"
	$ git push origin main
	```

1. Notify relevant individuals that there are new transactions queued up, and that a ceremony should be scheduled. This can be automated in the future so that when a commit is made or PR opened, others are notified, for example using a incident management tool.

1. Tamper proof the AirgapOS and Air-gapped laptop

{{ #include ../../../../component-documents/tamper-evidence-methods.md:vsbwf-procedure-sealing}}
// ANCHOR_END: content
/* ANCHOR_END: all */
