# Solana: Create Transaction Payload

{{ #include template-create-tx-0.md:content }}

1. Use icepick to generate and sign the payload by running one of the following available workflows:
	#### Transfer
	Transfer native Solana asset - SOL.

	`$ icepick workflow sol transfer --to-address <to-address> --from-address <from-address> --amount <amount> --export-for-quorum --sign`

	#### Transfer Token
	Transfer SPL tokens on Solana blockchain.

	The following SPL tokens, provided to `--token-name`, are supported:

	* [PYTH](https://www.pyth.network/)

	`$ icepick workflow sol transfer-token --from-address <from-address> --to-address <to-address> --token-name <token-name> --token-amount <token-amount> --export-for-quorum --sign`

{{ #include template-create-tx-1.md:content }}
