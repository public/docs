/* ANCHOR: all */
// ANCHOR: content
## Requirements

* [Quorum PGP Key](../operator/quorum-entropy-ceremony.md)

* [Air-Gapped Bundle](../provisioner/air-gapped-bundle.md)

	* The proposer  should print photographic evidence from digital cameras which is stored in a PGP signed repository. The photographs should be of the top and underside of the vacuum sealed object.

	* The proposer should verify the commit signatures of the photographs they are printing against a list of permitted PGP keys found in the `vaults` repo

{{ #include ../../../../component-documents/linux-workstation.md:content }}

* Clone the [Vaults Repository](../../../all-levels/create-vaults-repository.md) for your organization to the machine

## Procedure

1. Turn on online linux workstation

1. Clone the `vaults` repository if it's not available locally and get the latest changes:
	```
	$ git clone <repository_git_url>
	$ git pull origin main
	```
1. Unseal the SD Card Pack

{{ #include ../../../../component-documents/tamper-evidence-methods.md:vsbwf-procedure-unsealing}}

1. Plug a fresh SD card into the online linux workstation

1. {{ #include ../../../../component-documents/finding-device-name.md:content }}

1. Save the `vaults` repo to the SD card, referred to as the Ceremony SD card
	```
	$ cp -r ~/vaults/ /media
	```
1. Unplug the Ceremony SD card

1. Unseal the tamper proofed bundle

{{ #include ../../../../component-documents/tamper-evidence-methods.md:vsbwf-procedure-unsealing}}

1. Insert the AirgapOS SD card into the airgapped machine and turn it on

1. Once booted, unplug the AirgapOS SD card and place it in High Visibility Storage

1. Plug in the Ceremony SD card

1. Copy the git repo locally from the Ceremony SD card and change to it
	```
	$ cp -r /media/vaults /root
	$ cd /root/vaults
	```
1. Create a new payloads directory in the `vaults` repository for the date on which the ceremony for the transaction will take place if it doesn't already exist

	* `mkdir -p <namespace>/ceremonies/<date>/payloads`

	* e.g `mkdir -p acme-coin-01/ceremonies/2025-01-01/payloads`

1. Use `icepick workflow --help` to list the available workflows and options

1. Plug in the Operator smart card
// ANCHOR_END: content
/* ANCHOR_END: all */
