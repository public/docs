# Kyve: Create Transaction Payload

{{ #include template-create-tx-0.md:content }}

1. Use icepick to generate and sign the payload by running one of the following available workflows:

	There may be some difficulty broadcasting a transaction due
	to the amount of gas consumed, as each Cosmos chain may have different
	computation power available. The option `--gas-factor` may be set to a
	number to multiply the gas by, such as `1.2`, to increase the amount of gas
	for a transaction. The default value is `1`, and may be omitted if desired.
	A value lower than 1 is not recommended.

	The option `--chain-name` can use `kyve`, `kaon` (testnet), and `korellia` (devnet)

	#### Stake
	Stake coins on the provided chain towards a validator operator's address.

	`$ icepick workflow cosmos stake --delegate-address <delegate-address> --validator-address <validator-address> --chain-name kyve --asset-name KYVE --asset-amount <asset-amount> --gas-factor <gas-factor> --export-for-quorum --sign`

	#### Transfer
	Transfer coins on the cosmos blockchain.

	`$ icepick workflow cosmos transfer --from-address <from-address> --to-address <to-address> --chain-name kyve --asset-name <asset-name> --asset-amount <asset-amount> --export-for-quorum --sign`

	#### Withdraw
	Withdraw staked coins from a validator. Staked coins may be held for an unbonding period, depending on the chain upon which they are staked.

	`$ icepick workflow cosmos withdraw --delegate-address <delegate-address> --validator-address <validator-address> --chain-name kyve --asset-name KYVE --gas-factor <gas-factor> --export-for-quorum-sign`


	#### Withdraw Rewards
	Withdraw rewards gained from staking to a validator.

	`$ icepick workflow cosmos withdraw-rewards --delegate-address <delegate-address> --validator-address <validator-address> --chain-name kyve --gas-factor <gas-factor> --export-for-quorum-sign`

{{ #include template-create-tx-1.md:content }}
