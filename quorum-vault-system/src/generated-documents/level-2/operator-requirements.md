/* ANCHOR: all */
# Base Requirements

## For Quorum Based Operations
// ANCHOR: requirements

* For ALL tamper proofed hardware used in the ceremony, both operators MUST print photographic evidence from digital cameras which is stored in a PGP signed repository. The photographs should be of the top and underside of the vacuum sealed object.

    * The operators should verify the commit signatures of the photographs they are printing against a list of permitted PGP keys found in the "ceremonies" repo

* [AirgapOS SD card](/generated-documents/level-2/fixed-location/provisioner/provision-airgapos.md)
  * Provided by [Air-Gapped Bundle](/generated-documents/level-2/fixed-location/provisioner/air-gapped-bundle.md)
  * Alternative: Create your own from documentation in [AirgapOS Repository](https://git.distrust.co/public/airgap)

* AirgapOS Laptop
  * Provided by [Air-Gapped Bundle](/generated-documents/level-2/fixed-location/provisioner/air-gapped-bundle.md)
  * Alternative: Computer that can load AirgapOS ([compatibility reference](https://git.distrust.co/public/airgap#tested-models))

* Minimum of 1 [Operator](/system-roles.md#operator) and 1 [Witness](/system-roles.md#witness)

    * [Personal PGP key pair](/key-types.md#personal-pgp-keypair) for each operator

* Tamper-proofing equipment

// ANCHOR_END: requirements
/* ANCHOR_END: all */
