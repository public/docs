/* ANCHOR: all */
# Basic Requirements

## For Quorum Based Operations
// ANCHOR: requirements

* 2 individuals with appropriate role 

    * Each needs a [Personal PGP key pair](/generated-documents/all-levels/pgp-key-provisioning.html) 

* [Tamper-proofing equipment](/generated-documents/level-2/fixed-location/procurer/procure-tamper-proofing-equipment.html)

// ANCHOR_END: requirements
/* ANCHOR_END: all */