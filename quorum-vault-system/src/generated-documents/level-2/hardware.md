/* ANCHOR: all */
# Hardware for Level 2 Threat Model

## Computers 

* Computers for this use are are appropriate as long as they are compatible with AirgapOS. At this level, the essential aspect of hardware procurement is to ensure dual custody at all times. Outside of that any additional protections are welcome but not necessary.

* Laptops with chargers over ports which don't allow data transfer is preferred (non USB etc.)

// ANCHOR: computer-models 

* HP 13" Intel Celeron - 4GB Memory - 64GB eMMC, HP 14-dq0052dx, SKU: 6499749, UPC: 196548430192, DCS: 6.768.5321, ~USD $179.99
    * [Illustrated Parts Catalog](https://h10032.www1.hp.com/ctg/Manual/c04501162.pdf#%5B%7B%22num%22%3A3160%2C%22gen%22%3A0%7D%2C%7B%22name%22%3A%22XYZ%22%7D%2Cnull%2C732%2Cnull%5D)

* Lenovo 14" Flex 5i FHD Touchscreen 2-in-1 Laptop - Intel Core i3-1215U - 8GB Memory - Intel UHD Graphics, SKU: 6571565, ~USD $379.99
    * [Disassembly Manual](https://www.insidemylaptop.com/how-to-disassemble-lenovo-ideapad-flex-5-1470-model-81c9/)

* [Purism Librem 14](https://puri.sm/products/librem-14/)

* [Nova Custom](https://novacustom.com/de/) (Untested)

* [NitroPad](https://shop.nitrokey.com/shop?&search=nitropad) (Untested)

* Computers which are compatible which can be verified via [this guide](https://git.distrust.co/public/airgap#hardware-compatibility)

// ANCHOR_END: computer-models

## Digital Camera
// ANCHOR: camera-models

* MUST have >10MP

// ANCHOR_END: camera-models

### Models
// ANCHOR:digital-cameras

* Modern phone cameras

* [Kodak PIXPRO Friendly Zoom FZ43-BK 16MP Digital Camera with 4X Optical Zoom and 2.7" LCD Screen](https://www.amazon.com/Kodak-Friendly-FZ43-BK-Digital-Optical/dp/B01CG62D00)
    
* [Kodak PIXPRO Friendly Zoom FZ43-BK 16MP Digital Camera with 4X Optical Zoom and 2.7" LCD Screen](https://www.amazon.com/KODAK-Friendly-FZ45-BK-Digital-Optical/dp/B0B8PDHRWY)

* [Sony Cyber-Shot DSC-W800](https://www.amazon.com/Sony-DSCW800-Digital-Camera-Black/dp/B00I8BIBCW)

// ANCHOR_END:digital-cameras
/* ANCHOR_END: all */