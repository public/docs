# Q & A

## Is there a risk associated with deriving for different cryptographic algorithms from the same source of entropy?

This is avoided by using hardened hierarchical deterministic derivation which
is designed to ensure that each node in the derivation tree is isolated.

