# Locations

Locations refer to physical points in space which are used for storing
cryptographic material or performing actions using the cryptographic material and
adhere to a set of criteria which focus on achieving a high level of security -
specifically with respect to:

* Protecting access to devices which store cryptographic material

* Mitigating the risk stemming from natural disaster and other black swan events
such as civil unrest or war.

* Reducing the risk of exposing cryptographic material, for example via
side-channel attacks

There are three sub-types of Locations, one which is used for performing
any actions related to the management of the cryptographic material life-cycle
and is referred to as the Management Location, one for long term secure
storage of cryptographic material such as Smart Cards which are used to decrypt
[Shards](glossary.md#shard), referred to as a Storage Location, and a location
for Ceremonies, known as the Ceremony Location.

## Level 1

This level of defenses is largely focused on remote attacks, and as such does not have strict requirements about the location. 

### Examples

* Personal domicile

* Co-working space

* Regular office (non specific to QVS)

### Reference Design 

* SHOULD have ability to control physical access to room

* SHOULD be a space that's randomly selected to minimize the likelihood of an adversary deploying equipment into the location before it's used

## Level 2

This level of defenses is focused on insider threats and as such requires a considerably higher standard as it needs to mitigate threats which stem from individuals who have privileged access.

### Examples

* Purpose specific facility for QVS

* Short term rental

* Hotel room 

* Moving vehicle

### Reference Design

* MUST have physical access restrictions which require identification

* MUST have the ability to require more than 1 person to gain access

    * This control can be both physical, for example in vaults which require 2
    keys for access AND/OR process level, where the personnel of the facility
    may verify the identity of one or more individuals

* SHOULD have anti-fire systems

* SHOULD have anti-flood systems

* SHOULD be in facilities controlled by organizations which are ideally immune to being legally subpoenaed

## Level 3 

* MUST have anti-fire systems

* MUST have anti-flood systems

* MUST have 24/7 security monitoring

* MUST be in different geographic locations

    * This ensures that natural disasters are not likely to impact multiple
    locations simultaneously

* SHOULD be facilities owned by different organizations to reduce the risk of
collusion unless the organization who owns the QVS system has their own facility such
as a [SCIF](glossary.md#secure-compartmentalized-information-facility-scif).

## Level 4 (SCIF)

* MUST not have cameras installed inside of the room

* MUST not have windows with direct line of sight to monitors

* MUST have all walls protected with EM shielding which adheres to the TEMPEST
standard NATO SDIP-27 Level A

* SHOULD have seismic detectors