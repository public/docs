# Quorum Key Management (QVS)

Quorum Key Management (QVS) is an open source system of playbooks and tooling which
facilitates the creation and maintenance of highly resilient Quorum-based Key
Management Systems based on a strict threat model which can be used for a
variety of different cryptographic algorithms.

## Usage

In order to read the `mdbook` run the following commands:

```
cargo install mdbook
mdbook serve
```
