#!/bin/bash

COMMIT_HASH=$(git rev-parse --short HEAD)

echo "Commit Hash: $COMMIT_HASH" > src/commit_hash.md

# Build the mdBook
mdbook build