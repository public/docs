 1. Hardware decryption with user interaction
   * Tools:
     * Password Store
      * https://www.passwordstore.org/
     * Shared git repo
     * Yubikey with PGP keychain for each engineer
   * Defense:
     * Prevent theft of secrets not currently being used
   * Usage:
     * Encrypt secrets to Yubikey PGP keys of all holders as individual files
     * Place secrets in Git repo
     * Use "pass" command to sync and decrypt secrets on demand as needed
       * ```some-signing-command --key=<(pass Exodus/somesecret)```
     * Each access requires a Yubikey tap to decrypt
 2. Hardware decryption with explicit user consent
   * Tools:
     * Mooltipass
       * https://www.themooltipass.com/
     * Ledger
       * https://support.ledger.com/hc/en-us/articles/360017501380-Passwords?docs=true
     * Trezor
       * https://trezor.io/passwords/
   * Defense:
     * Prevent theft of secrets not currently being used
     * Prevent operator from being tricked into revealing wrong secret
   * Usage:
     * All devices use a pin to unlock, and can share encrypted databases
     * All devices explicitly ask for consent to release a secret by name
     * User reads on external display and approves with a button press
 3. Shamirs Secret Sharing to tamper evident system
   * Tools:
      * Remotely attestable TEE or HSM
        * Nitro Enclave
        * Google Confidential Compute
        * osresearch/heads booted server
   * Defense:
     * Prevent theft of secrets not currently being used
     * Prevent operator from being tricked into revealing wrong secret
     * Prevent compromised operator from stealing any secrets
   * Usage:
        * Public keys of trusted quorum provided to enclave
        * Secrets are created in enclave
        * Secrets are split into share requiring M-of-N to reconstruct
        * Enclave renturns shares encrypted to each quorum member public key
        * M-of-N quorum members can submit shares of given secret to servers
