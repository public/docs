# Crypto-Asset Custody Threat Modeling

## Summary

This document seeks to outline a broad set of threat models and risk mitigation
suggestions for crypto-asset custodians based on lessons learned from
historical failures to understand and remove attack surface.

It will also assume that not everyone has equal resources or equal risk and
as such four incrementally harder security levels to that effect, depending on
your budget and the expected sophistication of your worst case threat.

## Level 1

### Threat Model

* Adversary is as low skilled individual targeting users of many custodians.
* Adversary can:
  * Use phishing to steal text from a random set of custodian users
  * Inject malware into the systems of a random set of custodian users

### Requirements

* Large withdraw permissions MUST require hardware anchored login
* Large withdraw requests MUST be require hardware anchored signature
* Large withdraw requests MUST be verified on transaction signing system

### Reference Design

* Ensure all users wishing to withdraw significant value short period are using FIDO2 or PGP capable smartcard.
  * E.G. Webauthn: Android 7.0+, iOS 14+, MacOS 10.15+, Win10 1809+, ChromeOS, Yubikey 5, Nitrokey, Ledger, Trezor
  * Consider software-based WebAuthn/U2F as a backup
* Ensure backend systems will only approve large withdrawls if signed by known hardware token.
* Ensure all transacton approval keys are stored in a tamper evident append only database.
* Ensure all key additions are authenticated with old key
* Consider allowing quorum of support engineer keys to enroll a new key to handle lost keys
* Use hash of transaction signing request as challenge in to be signed by smartcard
* Blockchain signature only issued after verification a given request is signed by authorized user smartcard(s)

## Level 2

### Threat Model

* Adversary is a skilled and resourceful individual targeting one custodian.
* Adversary can:
  * Compromise one production engineer.
  * Inject code into any OSS library without well funded maintainers
  * Exploit any vulnerability within 24h of public knowledge

### Requirements

* All production access:
  * MUST NOT be possible by any single engineer
    * Consider a bastion that can enforce m-of-n access over SSH or similar
  * MUST be via dedicated tamper evident operating sytems
    * Consider: https://github.com/hashbang/book/blob/master/content/docs/security/Production_Engineering.md
  * MUST be anchored to keys in dedicated HSMs held by each administrator
    * Consider GnuPG or PKSC#11 devices that support touch-approval for ssh
* Any code in the transaction signing trust supply chain:
  * MUST build deterministically
  * MUST have extensive and frequent review.
    * Example: The Linux kernel has well funded distributed review.
  * MUST be signed in version control systems by well known author keys
  * MUST be signed by separate subject matter expert after security review
    * If third party code: it MUST be hash-pinned at known reviewed versions
  * MUST be at version with all known related security patches
  * SHOULD be latest versions if security disclosures lag behind releases
    * Example: The Linux kernel
  * MUST be built and signed by multiple parties with no management overlay
    * Example: One build by IT, another by Infrastructure team managed CI/CD
  * MUST be signed by well known keys signed by a common CA
    * Example: PGP Smartcards signed under OpenPGP-CA.
  * All private keys involved:
    * MUST NOT ever come in contact with network accessible memory
  * All execution environments MUST be able to attest what binary they run
    * Examples:
      * Custom Secure Boot verifies minimum signatures against CA
      * Cloud enclave that can remotely attest it uses a multi-signed image
        * AWS Nitro Enclave, Google Sheilded VMs, etc.
      * App phone stores already anchor to developer held signing keys

### Reference Design
  * Extend reference design from Level 1
  * Create offline CA key(s)
    * Consider OpenGPG key generated on airgap, backed up, and copies transmitted to a smartcards such as a Yubikey
  *CA key smartards are stored in dual-access tamper evident locations
  * User key management secure enclave is created
    * Enclave is immutable with no ingress internet access
    * Enclave has random ephemeral key
    * Remotely attested on bootup againest multi-signed and known determinisically built system image
      * Possible on many PCR based measured boot solutions like Heads, AWS Nitro Enclaves, or GCP Shielded VMs
    * Ephemeral enclave key is signed with offline CA key(s) on verification.
    * Enclave has ability to validate append only database of keys
    * Enclave will sign new key additions/removals with ephemeral key if:
      * User has no prior keys
      * Key was signed with an existing key
      * Key was signed with 2+ known support enginneer keys
  * Signing key generation
    * M-of-N keyholder quorum is selected
      * Should ideally be on different teams
      * Should ideally live geographically separated
      * Should have their own OpenPGP smartcard with pin and keys only they control.
    * Shard keys generated
      * Should be an additional OpenPGP smartcard separate from holders personal key
      * Should have random pin, encrypted to a backup shard holder 
      * Should be stored in a neutral location only the primary and backup shardholder can access
    * Done in person on airgap laptop that has been in dual witnessed custody since procurement
      * Has hardware anchor that can make all parties confient the OS image it is running is expected (Heads, etc)
      * Has two hardware sources of entropy
      * Runs known deterministic and immutable OS image compiled by multiple parties
    * Key is generated and stored
      * Split to m-of-n shamirs secret sharing shards
        * Each shard is encrypted to dedicated shard OpenPGP Smartcard
        * Shard smartcard pin is generated randomly
        * Shard smartcard pin is encypted to personal smartcards of primary and backup holders
  * Signing enclave is created
    * Is immutable with no ingress internet access
    * Has random ephemeral key
    * Remotely attested on bootup against multi-signed and known determinisically built system image
    * Will accept shamirs secret sharing shards encrypted to it
    * Will restore signing key to memory when sufficient shards are submitted
    * Will only sign transactions if accompanied by signed request by authorized user
      * Is able to validate signing request via CA key authorized user key management enclave signature
    * Will only sign transactions that meet predefined size and rate limits by company policy and insurance levels. 

## Level 3

### Threat Model

* Adversary is an organized group with significant funding.
* Adversary can:
  * Compromise one datacenter engineer into tampering with a target system
  * Use a sophisticated 0day to compromise any one internet connected system

### Requirements

* All transactions MUST be signed by multiple locations to be valid on chain
 * Consider open source and well vetted MPC or on-chain threshold signing
 * Locations MUST be separated by hours of travel
 * Locations MUST not have any staff overlap
 * Signing locations SHOULD distrust other locations
   * Each location SHOULD do their own reproducible build validation
   * Each location SHOULD do their own verifications on all large transactions

## Level 4

### Threat Model

* Adversary is a state actor.
* Adversary can:
  * Tamper with the supply chain of any single hardware/firmware component
  * Quickly relocate any device to a lab environment
  * Use sophisticated key exfiltration tactics
    * Acoustic (DiskFiltration, Fansmitter, Acoustic Cryptanalysis)
    * Optical (VisiSploit, xLED)
    * Thermal (BitWhisper)
    * Electrical (Differential Power Analysis, Power usage monitoring)
    * Magnetic (ODINI)
    * Electromagnetic (USBee)
    * Radio (NFCDrip, Vapor Trail, RFID, FM, Wifi, Bluetooth, GSM, etc)
    * Steganography
    * Non-deterministic encryption/signatures/data
    * Differential fault analysis
    * Data remanence

### Requirements

* All signing systems:
  * MUST have dual implementations of all policy enforcement and signing logic
  * MUST use two or more unrelated hardware supply chains
    * Example: Rust on RISC-V Linux on an FPGA vs C on PPC Gemalto enclave
  * MUST return deterministic results
    * Results are only exported for chain broadcast if identical
  * MUST be stored in near zero emissions vaults a single user can't open
    * See: NSA TEMPEST
  * MUST only communicate with outside world via fiber optic serial terminal
  * MUST be housed in Class III bank vault or better
  * MUST have constant environment deviation monitoring
    * Thermal, Acoustic, Air quality, Optical
  * MUST destroy key material on significant environment deviations.
  * MUST be accessible physically with cooperative physical access
    * Consider FF-L-2740B or better locks with dual pin enforcement
    * Consider dual biometric enforcement to get near area and disarm security
 
