# Share Recovery

## Overview

This document outlines the creation of a "Share Recovery" (SR) system which
functions as a one-way box that one can encrypt a partial secret to at any
time, with decryption only possible by a share holder with access to an offline
encryption key.

Such system offers high security, but low redundancy. It is suitable for
encrypting only a single share of a separate disaster recovery system that
requires m-of-n portions of data in order to recover.

Data is backed up by encrypting plaintext to a [Share Recovery Key](#sr-key).
The resulting ciphertext is then stored in the
[Share Recovery Warehouse](#sr-warehouse). In the case of a disaster,
ciphertexts can be gathered from the SR Warehouse and then decrypted using the
SR Key to regain access to the plaintext, which can be combined with shares
from other systems to reconstitute desired data by the data owner.

## Threat Model

- An adversary with any type of online attack is tolerated
  - Management of key and share material is managed entirely offline
  - Offline systems are heavily controlled for supply chain integrity
- Coercion of a single operator is tolerated
  - Share holder will never have access to more than one share
  - We expect this is unlikely to happen to two share holder at once
- Destruction of a single share is tolerated
  - This is only a single share in a redundant system
  - We expect the destruction of multiple shares at once is unlikely
  - We expect shares are sufficiently geographically distributed

## Components

### Share Owner
  - The owner of the share data encrypted to the Share Recovery System
  - Could differ from the entity which initially provides the share

### DR System
  - External DR system utilizing requiring multiple secrets to operate
    - Examples: Threshold signing, MPC, or Shamir's Secret Sharing

### SR Key

- PGP asymmetric key pair a single DR System secret is directly encrypted to
- Only accessible by one or more SR Operators
- Generated offline by an SR Operator using standard PGP processes
- Stored on a dedicated pin controlled HSM
- We chose the PGP standard because:
  - It is a widely supported with a plurality of implementations and tooling
  - The PGP standard and tooling is assumed to outlive any custom made tools
  - Should be more reliable than any crypto implementation we maintain

### SR Pin
- Pin that controls access to the HSM containing the SR Key

### SR Ciphertext

- Encrypted Ciphertext of a secret encrypted to the SR Key

### SR Location

- SR Key and SR Ciphertext storage location
  - The Location must be geograhically separate from other Shares in DR system
  - The SR Location has a fixed human access list
    - Those with access can however cooperate to transfer access to others
  - The SR Location has physical controls that strictly enforce access
    - E.G. A safety deposit box, TL-15+ Safe, etc.

### SR Operator

- A human who is on the access list for an SR Location
- Must be highly trusted, but does not have to highly technically skilled
- A human who is capable of decrypting data with a SR Key

### SR Warehouse

- Online storage for encrypted data replicated across multiple providers
- All data in SR Warehouse can only be decrypted by the SR Key
- Tolerate loss of any single provider by duplicating data to all of them
- Storage backends can be any combination of the following:
    - S3 Compatible object stores:
      - AWS, Google Cloud, DigitalOcean, Azure, etc.
    - Version control systems:
      - Github, AWS CodeCommit, Gitea
- We tolerate a loss of all but one SR storage backend
- A minimum of three storage backends must be maintained

### Airgap OS

- QubesOS Vault VM, or dedicated immutable Linux distro such as AirgapOS:
  - <https://github.com/distrust-foundation/airgapos>
- Basic shell tools
  - bash, sed, grep, awk, etc.
- Cryptography tools
  - gpg, openssl, etc

### SR Decryption Script

**Routine Inputs:**

- SR Ciphertext
- SR Key PIN
- SR Key HSM

**Routine:**

1. Operator invokes script to decrypt given SR Ciphertext on Airgap OS
2. Operator is prompted for SR Key HSM and SR Key Pin

**Routine Output:**

- Share in plaintext

### Share Storage Process

  1. Operator creates a dedicated SR Key
  2. Operator backs encrypted copy of SR key to SR Warehouse
  3. Operator transports SR Smartcard to SR Location.
  4. operator provides public SR Key to Share Owner or designated entity
  5. Share Owner creates and retains a sha256 hash of plaintext share
  6. Share Owner creates SR ciphertext by encrypting Share to SR Key
  7. SR Ciphertext is provided to an Operator
  8. Operator executes Share Recovery Process to decrypt SR Ciphertext
  9. Operator creates sha256 hash of the contents of the SR Ciphertext
  10. Operator backs up SR Ciphertext to SR Warehouse
  11. Operator returns sha256 hash to Share Owner
  12. Share owner confirms sha256 hash, proving decryption was successful

### Share Recovery Process

  1. A Share Owner submits a request for plaintext share
  2. An Operator verifies the identity of the Share Owner using multiple means
    - Ideally verify a signed request with a well known key
    - Verify in person or over video call
  3. An Operator obtains required resources
    - Airgap OS on hardware trusted by Operator
    - SR Key
    - SR Key Pin
    - SR Ciphertext
  3. Operator executes SR Decryption script
  4. Plaintext is provided to requesting Share Owner via requested means
    - Ideally immediately re-encrypted to a key controlled by Share Owner
